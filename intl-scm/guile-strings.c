/* src/app-utils/date-utilities.scm */
_("%s to %s")
/* src/app-utils/date-utilities.scm */
_("Start of this year")
/* src/app-utils/date-utilities.scm */
_("First day of the current calendar year")
/* src/app-utils/date-utilities.scm */
_("End of this year")
/* src/app-utils/date-utilities.scm */
_("Last day of the current calendar year")
/* src/app-utils/date-utilities.scm */
_("Start of previous year")
/* src/app-utils/date-utilities.scm */
_("First day of the previous calendar year")
/* src/app-utils/date-utilities.scm */
_("End of previous year")
/* src/app-utils/date-utilities.scm */
_("Last day of the previous calendar year")
/* src/app-utils/date-utilities.scm */
_("Start of next year")
/* src/app-utils/date-utilities.scm */
_("First day of the next calendar year")
/* src/app-utils/date-utilities.scm */
_("End of next year")
/* src/app-utils/date-utilities.scm */
_("Last day of the next calendar year")
/* src/app-utils/date-utilities.scm */
_("Start of accounting period")
/* src/app-utils/date-utilities.scm */
_("First day of the accounting period, as set in the global preferences")
/* src/app-utils/date-utilities.scm */
_("End of accounting period")
/* src/app-utils/date-utilities.scm */
_("Last day of the accounting period, as set in the global preferences")
/* src/app-utils/date-utilities.scm */
_("Start of this month")
/* src/app-utils/date-utilities.scm */
_("First day of the current month")
/* src/app-utils/date-utilities.scm */
_("End of this month")
/* src/app-utils/date-utilities.scm */
_("Last day of the current month")
/* src/app-utils/date-utilities.scm */
_("Start of previous month")
/* src/app-utils/date-utilities.scm */
_("First day of the previous month")
/* src/app-utils/date-utilities.scm */
_("End of previous month")
/* src/app-utils/date-utilities.scm */
_("Last day of previous month")
/* src/app-utils/date-utilities.scm */
_("Start of next month")
/* src/app-utils/date-utilities.scm */
_("First day of the next month")
/* src/app-utils/date-utilities.scm */
_("End of next month")
/* src/app-utils/date-utilities.scm */
_("Last day of next month")
/* src/app-utils/date-utilities.scm */
_("Start of current quarter")
/* src/app-utils/date-utilities.scm */
_("First day of the current quarterly accounting period")
/* src/app-utils/date-utilities.scm */
_("End of current quarter")
/* src/app-utils/date-utilities.scm */
_("Last day of the current quarterly accounting period")
/* src/app-utils/date-utilities.scm */
_("Start of previous quarter")
/* src/app-utils/date-utilities.scm */
_("First day of the previous quarterly accounting period")
/* src/app-utils/date-utilities.scm */
_("End of previous quarter")
/* src/app-utils/date-utilities.scm */
_("Last day of previous quarterly accounting period")
/* src/app-utils/date-utilities.scm */
_("Start of next quarter")
/* src/app-utils/date-utilities.scm */
_("First day of the next quarterly accounting period")
/* src/app-utils/date-utilities.scm */
_("End of next quarter")
/* src/app-utils/date-utilities.scm */
_("Last day of next quarterly accounting period")
/* src/app-utils/date-utilities.scm */
_("Today")
/* src/app-utils/date-utilities.scm */
_("The current date")
/* src/app-utils/date-utilities.scm */
_("One Month Ago")
/* src/app-utils/date-utilities.scm */
_("One Month Ago")
/* src/app-utils/date-utilities.scm */
_("One Week Ago")
/* src/app-utils/date-utilities.scm */
_("One Week Ago")
/* src/app-utils/date-utilities.scm */
_("Three Months Ago")
/* src/app-utils/date-utilities.scm */
_("Three Months Ago")
/* src/app-utils/date-utilities.scm */
_("Six Months Ago")
/* src/app-utils/date-utilities.scm */
_("Six Months Ago")
/* src/app-utils/date-utilities.scm */
_("One Year Ago")
/* src/app-utils/date-utilities.scm */
_("One Year Ago")
/* src/app-utils/date-utilities.scm */
_("One Month Ahead")
/* src/app-utils/date-utilities.scm */
_("One Month Ahead")
/* src/app-utils/date-utilities.scm */
_("One Week Ahead")
/* src/app-utils/date-utilities.scm */
_("One Week Ahead")
/* src/app-utils/date-utilities.scm */
_("Three Months Ahead")
/* src/app-utils/date-utilities.scm */
_("Three Months Ahead")
/* src/app-utils/date-utilities.scm */
_("Six Months Ahead")
/* src/app-utils/date-utilities.scm */
_("Six Months Ahead")
/* src/app-utils/date-utilities.scm */
_("One Year Ahead")
/* src/app-utils/date-utilities.scm */
_("One Year Ahead")
/* src/app-utils/prefs.scm */
_("Funds In")
/* src/app-utils/prefs.scm */
_("Deposit")
/* src/app-utils/prefs.scm */
_("Receive")
/* src/app-utils/prefs.scm */
_("Payment")
/* src/app-utils/prefs.scm */
_("Increase")
/* src/app-utils/prefs.scm */
_("Decrease")
/* src/app-utils/prefs.scm */
_("Buy")
/* src/app-utils/prefs.scm */
_("Buy")
/* src/app-utils/prefs.scm */
_("Buy")
/* src/app-utils/prefs.scm */
_("Charge")
/* src/app-utils/prefs.scm */
_("Expense")
/* src/app-utils/prefs.scm */
_("Payment")
/* src/app-utils/prefs.scm */
_("Invoice")
/* src/app-utils/prefs.scm */
_("Decrease")
/* src/app-utils/prefs.scm */
_("Decrease")
/* src/app-utils/prefs.scm */
_("Funds Out")
/* src/app-utils/prefs.scm */
_("Withdrawal")
/* src/app-utils/prefs.scm */
_("Spend")
/* src/app-utils/prefs.scm */
_("Charge")
/* src/app-utils/prefs.scm */
_("Decrease")
/* src/app-utils/prefs.scm */
_("Increase")
/* src/app-utils/prefs.scm */
_("Sell")
/* src/app-utils/prefs.scm */
_("Sell")
/* src/app-utils/prefs.scm */
_("Sell")
/* src/app-utils/prefs.scm */
_("Income")
/* src/app-utils/prefs.scm */
_("Rebate")
/* src/app-utils/prefs.scm */
_("Bill")
/* src/app-utils/prefs.scm */
_("Payment")
/* src/app-utils/prefs.scm */
_("Increase")
/* src/app-utils/prefs.scm */
_("Increase")
/* src/business/business-utils/business-prefs.scm */
_("Counters")
/* src/business/business-utils/business-prefs.scm */
_("Customer number format")
/* src/business/business-utils/business-prefs.scm */
_("Customer number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating customer numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous customer number generated. This number will be incremented to generate the next customer number.")
/* src/business/business-utils/business-prefs.scm */
_("Employee number format")
/* src/business/business-utils/business-prefs.scm */
_("Employee number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating employee numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous employee number generated. This number will be incremented to generate the next employee number.")
/* src/business/business-utils/business-prefs.scm */
_("Invoice number format")
/* src/business/business-utils/business-prefs.scm */
_("Invoice number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating invoice numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous invoice number generated. This number will be incremented to generate the next invoice number.")
/* src/business/business-utils/business-prefs.scm */
_("Bill number format")
/* src/business/business-utils/business-prefs.scm */
_("Bill number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating bill numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous bill number generated. This number will be incremented to generate the next bill number.")
/* src/business/business-utils/business-prefs.scm */
_("Expense voucher number format")
/* src/business/business-utils/business-prefs.scm */
_("Expense voucher number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating expense voucher numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous expense voucher number generated. This number will be incremented to generate the next voucher number.")
/* src/business/business-utils/business-prefs.scm */
_("Job number format")
/* src/business/business-utils/business-prefs.scm */
_("Job number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating job numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous job number generated. This number will be incremented to generate the next job number.")
/* src/business/business-utils/business-prefs.scm */
_("Order number format")
/* src/business/business-utils/business-prefs.scm */
_("Order number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating order numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous order number generated. This number will be incremented to generate the next order number.")
/* src/business/business-utils/business-prefs.scm */
_("Vendor number format")
/* src/business/business-utils/business-prefs.scm */
_("Vendor number")
/* src/business/business-utils/business-prefs.scm */
_("The format string to use for generating vendor numbers. This is a printf-style format string.")
/* src/business/business-utils/business-prefs.scm */
_("The previous vendor number generated. This number will be incremented to generate the next vendor number.")
/* src/business/business-utils/business-prefs.scm */
_("The name of your business")
/* src/business/business-utils/business-prefs.scm */
_("The address of your business")
/* src/business/business-utils/business-prefs.scm */
_("The contact person to print on invoices")
/* src/business/business-utils/business-prefs.scm */
_("The phone number of your business")
/* src/business/business-utils/business-prefs.scm */
_("The fax number of your business")
/* src/business/business-utils/business-prefs.scm */
_("The email address of your business")
/* src/business/business-utils/business-prefs.scm */
_("The URL address of your website")
/* src/business/business-utils/business-prefs.scm */
_("The ID for your company (eg 'Tax-ID: 00-000000)")
/* src/business/business-utils/business-prefs.scm */
_("Default Customer TaxTable")
/* src/business/business-utils/business-prefs.scm */
_("The default tax table to apply to customers.")
/* src/business/business-utils/business-prefs.scm */
_("Default Vendor TaxTable")
/* src/business/business-utils/business-prefs.scm */
_("The default tax table to apply to vendors.")
/* src/business/business-utils/business-prefs.scm */
_("Fancy Date Format")
/* src/business/business-utils/business-prefs.scm */
_("The default date format used for fancy printed dates")
/* src/business/business-utils/business-prefs.scm */
_("Check to have trading accounts used for transactions involving more than one currency or commodity")
/* src/business/business-utils/business-prefs.scm */
_("Budget to be used when none has been otherwise specified")
/* src/business/business-utils/business-utils.scm */
_("Business")
/* src/business/business-utils/business-utils.scm */
_("Company Name")
/* src/business/business-utils/business-utils.scm */
_("Company Address")
/* src/business/business-utils/business-utils.scm */
_("Company ID")
/* src/business/business-utils/business-utils.scm */
_("Company Phone Number")
/* src/business/business-utils/business-utils.scm */
_("Company Fax Number")
/* src/business/business-utils/business-utils.scm */
_("Company Website URL")
/* src/business/business-utils/business-utils.scm */
_("Company Email Address")
/* src/business/business-utils/business-utils.scm */
_("Company Contact Person")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Business")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Name")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Address")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company ID")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Phone Number")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Fax Number")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Website URL")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Email Address")
/* src/business/business-utils/gnucash/business-utils.scm */
_("Company Contact Person")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Dividends")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Interest")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Cap Return")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Cap. gain (long)")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Cap. gain (mid)")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Cap. gain (short)")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Equity")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Retained Earnings")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Equity")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Retained Earnings")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Expenses")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Commissions")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Expenses")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Margin Interest")
/* src/import-export/qif-import/qif-dialog-utils.scm */
_("Unspecified")
/* src/import-export/qif-import/qif-file.scm */
_("Line")
/* src/import-export/qif-import/qif-file.scm */
_("Line")
/* src/import-export/qif-import/qif-file.scm */
_("Read aborted.")
/* src/import-export/qif-import/qif-file.scm */
_("Reading")
/* src/import-export/qif-import/qif-file.scm */
_("Some characters have been discarded.")
/* src/import-export/qif-import/qif-file.scm */
_("Converted to: ")
/* src/import-export/qif-import/qif-file.scm */
_("Some characters have been converted according to your locale.")
/* src/import-export/qif-import/qif-file.scm */
_("Converted to: ")
/* src/import-export/qif-import/qif-file.scm */
_("Ignoring unknown option")
/* src/import-export/qif-import/qif-file.scm */
_("Date required.")
/* src/import-export/qif-import/qif-file.scm */
_("Discarding this transaction.")
/* src/import-export/qif-import/qif-file.scm */
_("Ignoring class line")
/* src/import-export/qif-import/qif-file.scm */
_("Ignoring category line")
/* src/import-export/qif-import/qif-file.scm */
_("Ignoring security line")
/* src/import-export/qif-import/qif-file.scm */
_("File does not appear to be in QIF format")
/* src/import-export/qif-import/qif-file.scm */
_("Transaction date")
/* src/import-export/qif-import/qif-file.scm */
_("Transaction amount")
/* src/import-export/qif-import/qif-file.scm */
_("Share price")
/* src/import-export/qif-import/qif-file.scm */
_("Share quantity")
/* src/import-export/qif-import/qif-file.scm */
_("Investment action")
/* src/import-export/qif-import/qif-file.scm */
_("Reconciliation status")
/* src/import-export/qif-import/qif-file.scm */
_("Commission")
/* src/import-export/qif-import/qif-file.scm */
_("Account type")
/* src/import-export/qif-import/qif-file.scm */
_("Tax class")
/* src/import-export/qif-import/qif-file.scm */
_("Category budget amount")
/* src/import-export/qif-import/qif-file.scm */
_("Account budget amount")
/* src/import-export/qif-import/qif-file.scm */
_("Credit limit")
/* src/import-export/qif-import/qif-file.scm */
_("Parsing categories")
/* src/import-export/qif-import/qif-file.scm */
_("Parsing accounts")
/* src/import-export/qif-import/qif-file.scm */
_("Parsing transactions")
/* src/import-export/qif-import/qif-file.scm */
_("Unrecognized or inconsistent format.")
/* src/import-export/qif-import/qif-file.scm */
_("Parsing failed.")
/* src/import-export/qif-import/qif-file.scm */
_("Parse ambiguity between formats")
/* src/import-export/qif-import/qif-file.scm */
_("Value '%s' could be %s or %s.")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Dividends")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Interest")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Cap Return")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Cap. gain (long)")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Cap. gain (mid)")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Income")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Cap. gain (short)")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Equity")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Retained Earnings")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Equity")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Retained Earnings")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Expenses")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Commissions")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Expenses")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Margin Interest")
/* src/import-export/qif-import/qif-import/qif-dialog-utils.scm */
_("Unspecified")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Line")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Line")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Read aborted.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Reading")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Some characters have been discarded.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Converted to: ")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Some characters have been converted according to your locale.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Converted to: ")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Ignoring unknown option")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Date required.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Discarding this transaction.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Ignoring class line")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Ignoring category line")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Ignoring security line")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("File does not appear to be in QIF format")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Transaction date")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Transaction amount")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Share price")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Share quantity")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Investment action")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Reconciliation status")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Commission")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Account type")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Tax class")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Category budget amount")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Account budget amount")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Credit limit")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Parsing categories")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Parsing accounts")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Parsing transactions")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Unrecognized or inconsistent format.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Parsing failed.")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Parse ambiguity between formats")
/* src/import-export/qif-import/qif-import/qif-file.scm */
_("Value '%s' could be %s or %s.")
/* src/import-export/qif-import/qif-import/qif-merge-groups.scm */
_("Finding duplicate transactions")
/* src/import-export/qif-import/qif-import/qif-parse.scm */
_("Unrecognized account type '%s'. Defaulting to Bank.")
/* src/import-export/qif-import/qif-import/qif-parse.scm */
_("Unrecognized action '%s'.")
/* src/import-export/qif-import/qif-import/qif-parse.scm */
_("Unrecognized status '%s'. Defaulting to uncleared.")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("QIF import: Name conflict with another account.")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("Preparing to convert your QIF data")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("Creating accounts")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("Matching transfers between accounts")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("Converting")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("Missing transaction date.")
/* src/import-export/qif-import/qif-import/qif-to-gnc.scm */
_("Dates earlier than 1970 are not supported.")
/* src/import-export/qif-import/qif-merge-groups.scm */
_("Finding duplicate transactions")
/* src/import-export/qif-import/qif-parse.scm */
_("Unrecognized account type '%s'. Defaulting to Bank.")
/* src/import-export/qif-import/qif-parse.scm */
_("Unrecognized action '%s'.")
/* src/import-export/qif-import/qif-parse.scm */
_("Unrecognized status '%s'. Defaulting to uncleared.")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("QIF import: Name conflict with another account.")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("Preparing to convert your QIF data")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("Creating accounts")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("Matching transfers between accounts")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("Converting")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("Missing transaction date.")
/* src/import-export/qif-import/qif-to-gnc.scm */
_("Dates earlier than 1970 are not supported.")
/* src/report/business-reports/aging.scm */
_("To")
/* src/report/business-reports/aging.scm */
_("Sort By")
/* src/report/business-reports/aging.scm */
_("Sort Order")
/* src/report/business-reports/aging.scm */
_("Report's currency")
/* src/report/business-reports/aging.scm */
_("Price Source")
/* src/report/business-reports/aging.scm */
_("Show Multi-currency Totals")
/* src/report/business-reports/aging.scm */
_("Show zero balance items")
/* src/report/business-reports/aging.scm */
_("Due or Post Date")
/* src/report/business-reports/aging.scm */
_("Transactions relating to '%s' contain more than one currency.  This report is not designed to cope with this possibility.")
/* src/report/business-reports/aging.scm */
_("Sort companies by")
/* src/report/business-reports/aging.scm */
_("Name")
/* src/report/business-reports/aging.scm */
_("Name of the company")
/* src/report/business-reports/aging.scm */
_("Total Owed")
/* src/report/business-reports/aging.scm */
_("Total amount owed to/from Company")
/* src/report/business-reports/aging.scm */
_("Bracket Total Owed")
/* src/report/business-reports/aging.scm */
_("Amount owed in oldest bracket - if same go to next oldest")
/* src/report/business-reports/aging.scm */
_("Sort order")
/* src/report/business-reports/aging.scm */
_("Increasing")
/* src/report/business-reports/aging.scm */
_("0 -> $999,999.99, A->Z")
/* src/report/business-reports/aging.scm */
_("Decreasing")
/* src/report/business-reports/aging.scm */
_("$999,999.99 -> $0, Z->A")
/* src/report/business-reports/aging.scm */
_("Show multi-currency totals.  If not selected, convert all totals to report currency")
/* src/report/business-reports/aging.scm */
_("Show all vendors/customers even if they have a zero balance.")
/* src/report/business-reports/aging.scm */
_("Leading date")
/* src/report/business-reports/aging.scm */
_("Due Date")
/* src/report/business-reports/aging.scm */
_("Due date is leading")
/* src/report/business-reports/aging.scm */
_("Post Date")
/* src/report/business-reports/aging.scm */
_("Post date is leading")
/* src/report/business-reports/aging.scm */
_("Company")
/* src/report/business-reports/aging.scm */
_("Current")
/* src/report/business-reports/aging.scm */
_("0-30 days")
/* src/report/business-reports/aging.scm */
_("31-60 days")
/* src/report/business-reports/aging.scm */
_("61-90 days")
/* src/report/business-reports/aging.scm */
_("91+ days")
/* src/report/business-reports/aging.scm */
_("Total")
/* src/report/business-reports/aging.scm */
_("Total")
/* src/report/business-reports/aging.scm */
_("No valid account selected.  Click on the Options button and select the account to use.")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Total")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Assets Accounts")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Liability Accounts")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Equity Accounts")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Trading Accounts")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Retained Losses")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Retained Earnings")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Total Equity, Trading, and Liabilities")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("Imbalance Amount")
/* src/report/business-reports/balsheet-eg.eguile.scm */
_("<strong>Exchange Rates</strong> used for this report")
/* src/report/business-reports/balsheet-eg.scm */
_("Balance Sheet (eguile)")
/* src/report/business-reports/balsheet-eg.scm */
_("Report Title")
/* src/report/business-reports/balsheet-eg.scm */
_("Title for this report")
/* src/report/business-reports/balsheet-eg.scm */
_("Balance Sheet Date")
/* src/report/business-reports/balsheet-eg.scm */
_("1- or 2-column report")
/* src/report/business-reports/balsheet-eg.scm */
_("The balance sheet can be displayed with either 1 or 2 columns.  'auto' means that the layout will be adjusted to fit the width of the page.")
/* src/report/business-reports/balsheet-eg.scm */
_("Levels of Subaccounts")
/* src/report/business-reports/balsheet-eg.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/business-reports/balsheet-eg.scm */
_("Flatten list to depth limit")
/* src/report/business-reports/balsheet-eg.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/business-reports/balsheet-eg.scm */
_("Exclude accounts with zero total balances")
/* src/report/business-reports/balsheet-eg.scm */
_("Exclude non-top-level accounts with zero balance and no non-zero sub-accounts")
/* src/report/business-reports/balsheet-eg.scm */
_("Display accounts as hyperlinks")
/* src/report/business-reports/balsheet-eg.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/business-reports/balsheet-eg.scm */
_("Negative amount format")
/* src/report/business-reports/balsheet-eg.scm */
_("The formatting to use for negative amounts: with a leading sign, or enclosing brackets")
/* src/report/business-reports/balsheet-eg.scm */
_("Font family")
/* src/report/business-reports/balsheet-eg.scm */
_("Font definition in CSS font-family format")
/* src/report/business-reports/balsheet-eg.scm */
_("Font size")
/* src/report/business-reports/balsheet-eg.scm */
_("Font size in CSS font-size format (e.g. \"medium\" or \"10pt\")")
/* src/report/business-reports/balsheet-eg.scm */
_("Template file")
/* src/report/business-reports/balsheet-eg.scm */
_("The file name of the eguile template part of this report.  This file must be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/balsheet-eg.scm */
_("CSS stylesheet file")
/* src/report/business-reports/balsheet-eg.scm */
_("The file name of the CSS stylesheet to use with this report.  If specified, this file should be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/balsheet-eg.scm */
_("Extra Notes")
/* src/report/business-reports/balsheet-eg.scm */
_("Notes added at end of invoice -- may contain HTML markup")
/* src/report/business-reports/balsheet-eg.scm */
_("Report's currency")
/* src/report/business-reports/balsheet-eg.scm */
_("Price Source")
/* src/report/business-reports/balsheet-eg.scm */
_("Show Foreign Currencies")
/* src/report/business-reports/balsheet-eg.scm */
_("Display any foreign currency amount in an account")
/* src/report/business-reports/balsheet-eg.scm */
_("Commodities")
/* src/report/business-reports/balsheet-eg.scm */
_("Notes")
/* src/report/business-reports/balsheet-eg.scm */
_("Auto")
/* src/report/business-reports/balsheet-eg.scm */
_("Adjust the layout to fit the width of the screen or page")
/* src/report/business-reports/balsheet-eg.scm */
_("One")
/* src/report/business-reports/balsheet-eg.scm */
_("Display liabilities and equity below assets")
/* src/report/business-reports/balsheet-eg.scm */
_("Two")
/* src/report/business-reports/balsheet-eg.scm */
_("Display assets on the left, liabilities and equity on the right")
/* src/report/business-reports/balsheet-eg.scm */
_("Sign")
/* src/report/business-reports/balsheet-eg.scm */
_("Prefix negative amounts with a minus sign, e.g. -$10.00")
/* src/report/business-reports/balsheet-eg.scm */
_("Brackets")
/* src/report/business-reports/balsheet-eg.scm */
_("Surround negative amounts with brackets, e.g. ($100.00)")
/* src/report/business-reports/balsheet-eg.scm */
_("(Development version -- don't rely on the numbers on this report without double-checking them.<br>Change the 'Extra Notes' option to get rid of this message)")
/* src/report/business-reports/balsheet-eg.scm */
_("Imbalance")
/* src/report/business-reports/balsheet-eg.scm */
_("Orphan")
/* src/report/business-reports/balsheet-eg.scm */
_("Balance Sheet using eguile-gnc")
/* src/report/business-reports/balsheet-eg.scm */
_("Display a balance sheet (using eguile template)")
/* src/report/business-reports/business-reports.scm */
_("_Business")
/* src/report/business-reports/customer-summary.scm */
_("From")
/* src/report/business-reports/customer-summary.scm */
_("To")
/* src/report/business-reports/customer-summary.scm */
_("Income Accounts")
/* src/report/business-reports/customer-summary.scm */
_("Income Accounts")
/* src/report/business-reports/customer-summary.scm */
_("The income accounts where the sales and income was recorded.")
/* src/report/business-reports/customer-summary.scm */
_("Expense Accounts")
/* src/report/business-reports/customer-summary.scm */
_("Expense Accounts")
/* src/report/business-reports/customer-summary.scm */
_("The expense accounts where the expenses are recorded which are subtracted from the sales to give the profit.")
/* src/report/business-reports/customer-summary.scm */
_("Show Expense Column")
/* src/report/business-reports/customer-summary.scm */
_("Show the column with the expenses per customer")
/* src/report/business-reports/customer-summary.scm */
_("Show Company Address")
/* src/report/business-reports/customer-summary.scm */
_("Show your own company's address and the date of printing")
/* src/report/business-reports/customer-summary.scm */
_("Display Columns")
/* src/report/business-reports/customer-summary.scm */
_("Date")
/* src/report/business-reports/customer-summary.scm */
_("Reference")
/* src/report/business-reports/customer-summary.scm */
_("Type")
/* src/report/business-reports/customer-summary.scm */
_("Description")
/* src/report/business-reports/customer-summary.scm */
_("Amount")
/* src/report/business-reports/customer-summary.scm */
_("Show Lines with All Zeros")
/* src/report/business-reports/customer-summary.scm */
_("Show the table lines with customers which did not have any transactions in the reporting period, hence would show all zeros in the columns.")
/* src/report/business-reports/customer-summary.scm */
_("Sort Column")
/* src/report/business-reports/customer-summary.scm */
_("Choose the column by which the result table is sorted")
/* src/report/business-reports/customer-summary.scm */
_("Sort Order")
/* src/report/business-reports/customer-summary.scm */
_("Choose the ordering of the column sort: Either ascending or descending")
/* src/report/business-reports/customer-summary.scm */
_("Balance")
/* src/report/business-reports/customer-summary.scm */
_("Payment")
/* src/report/business-reports/customer-summary.scm */
_("Payment")
/* src/report/business-reports/customer-summary.scm */
_("Unknown")
/* src/report/business-reports/customer-summary.scm */
_("Total")
/* src/report/business-reports/customer-summary.scm */
_("Customer Name")
/* src/report/business-reports/customer-summary.scm */
_("Sort alphabetically by customer name")
/* src/report/business-reports/customer-summary.scm */
_("Profit")
/* src/report/business-reports/customer-summary.scm */
_("Sort by profit amount")
/* src/report/business-reports/customer-summary.scm */
_("Markup")
/* src/report/business-reports/customer-summary.scm */
_("Sort by markup (which is profit amount divided by sales)")
/* src/report/business-reports/customer-summary.scm */
_("Sales")
/* src/report/business-reports/customer-summary.scm */
_("Sort by sales amount")
/* src/report/business-reports/customer-summary.scm */
_("Expense")
/* src/report/business-reports/customer-summary.scm */
_("Sort by expense amount")
/* src/report/business-reports/customer-summary.scm */
_("Ascending")
/* src/report/business-reports/customer-summary.scm */
_("A to Z, smallest to largest")
/* src/report/business-reports/customer-summary.scm */
_("Descending")
/* src/report/business-reports/customer-summary.scm */
_("Z to A, largest to smallest")
/* src/report/business-reports/customer-summary.scm */
_("Invoice")
/* src/report/business-reports/customer-summary.scm */
_("Bill")
/* src/report/business-reports/customer-summary.scm */
_("Expense Report")
/* src/report/business-reports/customer-summary.scm */
_("Customer")
/* src/report/business-reports/customer-summary.scm */
_("Vendor")
/* src/report/business-reports/customer-summary.scm */
_("Employee")
/* src/report/business-reports/customer-summary.scm */
_("Report")
/* src/report/business-reports/customer-summary.scm */
_("Customer")
/* src/report/business-reports/customer-summary.scm */
_("Profit")
/* src/report/business-reports/customer-summary.scm */
_("Markup")
/* src/report/business-reports/customer-summary.scm */
_("Sales")
/* src/report/business-reports/customer-summary.scm */
_("Expense")
/* src/report/business-reports/customer-summary.scm */
_("No Customer")
/* src/report/business-reports/customer-summary.scm */
_("Total")
/* src/report/business-reports/customer-summary.scm */
_("%s %s - %s")
/* src/report/business-reports/customer-summary.scm */
_("No valid %s selected.  Click on the Options button to select a company.")
/* src/report/business-reports/customer-summary.scm */
_("Customer Summary")
/* src/report/business-reports/easy-invoice.scm */
_("Invoice Number")
/* src/report/business-reports/easy-invoice.scm */
_("Date")
/* src/report/business-reports/easy-invoice.scm */
_("Description")
/* src/report/business-reports/easy-invoice.scm */
_("Charge Type")
/* src/report/business-reports/easy-invoice.scm */
_("Quantity")
/* src/report/business-reports/easy-invoice.scm */
_("Unit Price")
/* src/report/business-reports/easy-invoice.scm */
_("Discount")
/* src/report/business-reports/easy-invoice.scm */
_("Taxable")
/* src/report/business-reports/easy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/easy-invoice.scm */
_("Total")
/* src/report/business-reports/easy-invoice.scm */
_("%")
/* src/report/business-reports/easy-invoice.scm */
_("T")
/* src/report/business-reports/easy-invoice.scm */
_("Custom Title")
/* src/report/business-reports/easy-invoice.scm */
_("A custom string to replace Invoice, Bill or Expense Voucher")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Date")
/* src/report/business-reports/easy-invoice.scm */
_("Display the date?")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Description")
/* src/report/business-reports/easy-invoice.scm */
_("Display the description?")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Charge Type")
/* src/report/business-reports/easy-invoice.scm */
_("Display the charge type?")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Quantity")
/* src/report/business-reports/easy-invoice.scm */
_("Display the quantity of items?")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Price")
/* src/report/business-reports/easy-invoice.scm */
_("Display the price per item?")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Discount")
/* src/report/business-reports/easy-invoice.scm */
_("Display the entry's discount")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Taxable")
/* src/report/business-reports/easy-invoice.scm */
_("Display the entry's taxable status")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/easy-invoice.scm */
_("Display each entry's total total tax")
/* src/report/business-reports/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/easy-invoice.scm */
_("Total")
/* src/report/business-reports/easy-invoice.scm */
_("Display the entry's value")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("My Company")
/* src/report/business-reports/easy-invoice.scm */
_("Display my company name and address?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("My Company ID")
/* src/report/business-reports/easy-invoice.scm */
_("Display my company ID?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Due Date")
/* src/report/business-reports/easy-invoice.scm */
_("Display due date?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Individual Taxes")
/* src/report/business-reports/easy-invoice.scm */
_("Display all the individual taxes?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Totals")
/* src/report/business-reports/easy-invoice.scm */
_("Display the totals?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Subtotal")
/* src/report/business-reports/easy-invoice.scm */
_("Display the subtotals?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("References")
/* src/report/business-reports/easy-invoice.scm */
_("Display the invoice references?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Billing Terms")
/* src/report/business-reports/easy-invoice.scm */
_("Display the invoice billing terms?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Billing ID")
/* src/report/business-reports/easy-invoice.scm */
_("Display the billing id?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Invoice Notes")
/* src/report/business-reports/easy-invoice.scm */
_("Display the invoice notes?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Payments")
/* src/report/business-reports/easy-invoice.scm */
_("Display the payments applied to this invoice?")
/* src/report/business-reports/easy-invoice.scm */
_("Display")
/* src/report/business-reports/easy-invoice.scm */
_("Invoice Width")
/* src/report/business-reports/easy-invoice.scm */
_("The minimum width of the invoice.")
/* src/report/business-reports/easy-invoice.scm */
_("Text")
/* src/report/business-reports/easy-invoice.scm */
_("Extra Notes")
/* src/report/business-reports/easy-invoice.scm */
_("Extra notes to put on the invoice (simple HTML is accepted)")
/* src/report/business-reports/easy-invoice.scm */
_("Thank you for your patronage")
/* src/report/business-reports/easy-invoice.scm */
_("Text")
/* src/report/business-reports/easy-invoice.scm */
_("Today Date Format")
/* src/report/business-reports/easy-invoice.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/easy-invoice.scm */
_("Payment, thank you")
/* src/report/business-reports/easy-invoice.scm */
_("Net Price")
/* src/report/business-reports/easy-invoice.scm */
_("Tax")
/* src/report/business-reports/easy-invoice.scm */
_("Total Price")
/* src/report/business-reports/easy-invoice.scm */
_("Amount Due")
/* src/report/business-reports/easy-invoice.scm */
_("REF")
/* src/report/business-reports/easy-invoice.scm */
_("Invoice")
/* src/report/business-reports/easy-invoice.scm */
_("Bill")
/* src/report/business-reports/easy-invoice.scm */
_("Expense Voucher")
/* src/report/business-reports/easy-invoice.scm */
_("%s #%d")
/* src/report/business-reports/easy-invoice.scm */
_("Date")
/* src/report/business-reports/easy-invoice.scm */
_("Due")
/* src/report/business-reports/easy-invoice.scm */
_("INVOICE NOT POSTED")
/* src/report/business-reports/easy-invoice.scm */
_("Billing ID")
/* src/report/business-reports/easy-invoice.scm */
_("Terms")
/* src/report/business-reports/easy-invoice.scm */
_("No valid invoice selected.  Click on the Options button and select the invoice to use.")
/* src/report/business-reports/easy-invoice.scm */
_("Easy Invoice")
/* src/report/business-reports/fancy-invoice.scm */
_("Invoice Number")
/* src/report/business-reports/fancy-invoice.scm */
_("Date")
/* src/report/business-reports/fancy-invoice.scm */
_("Description")
/* src/report/business-reports/fancy-invoice.scm */
_("Charge Type")
/* src/report/business-reports/fancy-invoice.scm */
_("Quantity")
/* src/report/business-reports/fancy-invoice.scm */
_("Unit Price")
/* src/report/business-reports/fancy-invoice.scm */
_("Discount")
/* src/report/business-reports/fancy-invoice.scm */
_("Taxable")
/* src/report/business-reports/fancy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/fancy-invoice.scm */
_("Total")
/* src/report/business-reports/fancy-invoice.scm */
_("%")
/* src/report/business-reports/fancy-invoice.scm */
_("T")
/* src/report/business-reports/fancy-invoice.scm */
_("Custom Title")
/* src/report/business-reports/fancy-invoice.scm */
_("A custom string to replace Invoice, Bill or Expense Voucher")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Date")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the date?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Description")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the description?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Action")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the action?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Quantity")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the quantity of items?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Price")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the price per item?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Discount")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the entry's discount")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Taxable")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the entry's taxable status")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/fancy-invoice.scm */
_("Display each entry's total total tax")
/* src/report/business-reports/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/fancy-invoice.scm */
_("Total")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the entry's value")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Individual Taxes")
/* src/report/business-reports/fancy-invoice.scm */
_("Display all the individual taxes?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Totals")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the totals?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("References")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the invoice references?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Billing Terms")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the invoice billing terms?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Billing ID")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the billing id?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Invoice Notes")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the invoice notes?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Payments")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the payments applied to this invoice?")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Minimum # of entries")
/* src/report/business-reports/fancy-invoice.scm */
_("The minimum number of invoice entries to display. (-1)")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Extra Notes")
/* src/report/business-reports/fancy-invoice.scm */
_("Extra notes to put on the invoice")
/* src/report/business-reports/fancy-invoice.scm */
_("Thank you for your patronage")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Payable to")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the Payable to: information")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Payable to string")
/* src/report/business-reports/fancy-invoice.scm */
_("The phrase for specifying to whom payments should be made")
/* src/report/business-reports/fancy-invoice.scm */
_("Make all cheques Payable to")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Company contact")
/* src/report/business-reports/fancy-invoice.scm */
_("Display the Company contact information")
/* src/report/business-reports/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/fancy-invoice.scm */
_("Company contact string")
/* src/report/business-reports/fancy-invoice.scm */
_("The phrase used to introduce the company contact")
/* src/report/business-reports/fancy-invoice.scm */
_("Direct all inquiries to")
/* src/report/business-reports/fancy-invoice.scm */
_("Payment, thank you")
/* src/report/business-reports/fancy-invoice.scm */
_("Net Price")
/* src/report/business-reports/fancy-invoice.scm */
_("Tax")
/* src/report/business-reports/fancy-invoice.scm */
_("Total Price")
/* src/report/business-reports/fancy-invoice.scm */
_("Amount Due")
/* src/report/business-reports/fancy-invoice.scm */
_("REF")
/* src/report/business-reports/fancy-invoice.scm */
_("Phone:")
/* src/report/business-reports/fancy-invoice.scm */
_("Fax:")
/* src/report/business-reports/fancy-invoice.scm */
_("Web:")
/* src/report/business-reports/fancy-invoice.scm */
_("Invoice")
/* src/report/business-reports/fancy-invoice.scm */
_("Bill")
/* src/report/business-reports/fancy-invoice.scm */
_("Expense Voucher")
/* src/report/business-reports/fancy-invoice.scm */
_("%s&nbsp;#")
/* src/report/business-reports/fancy-invoice.scm */
_("%s&nbsp;Date")
/* src/report/business-reports/fancy-invoice.scm */
_("Due Date")
/* src/report/business-reports/fancy-invoice.scm */
_("Invoice in progress...")
/* src/report/business-reports/fancy-invoice.scm */
_("Reference")
/* src/report/business-reports/fancy-invoice.scm */
_("Terms")
/* src/report/business-reports/fancy-invoice.scm */
_("No valid invoice selected.  Click on the Options button and select the invoice to use.")
/* src/report/business-reports/fancy-invoice.scm */
_("Fancy Invoice")
/* src/report/business-reports/gnucash/report/aging.scm */
_("To")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Sort By")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Sort Order")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Report's currency")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Price Source")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Show Multi-currency Totals")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Show zero balance items")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Due or Post Date")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Transactions relating to '%s' contain more than one currency.  This report is not designed to cope with this possibility.")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Sort companies by")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Name")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Name of the company")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Total Owed")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Total amount owed to/from Company")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Bracket Total Owed")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Amount owed in oldest bracket - if same go to next oldest")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Sort order")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Increasing")
/* src/report/business-reports/gnucash/report/aging.scm */
_("0 -> $999,999.99, A->Z")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Decreasing")
/* src/report/business-reports/gnucash/report/aging.scm */
_("$999,999.99 -> $0, Z->A")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Show multi-currency totals.  If not selected, convert all totals to report currency")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Show all vendors/customers even if they have a zero balance.")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Leading date")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Due date is leading")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Post Date")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Post date is leading")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Company")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Current")
/* src/report/business-reports/gnucash/report/aging.scm */
_("0-30 days")
/* src/report/business-reports/gnucash/report/aging.scm */
_("31-60 days")
/* src/report/business-reports/gnucash/report/aging.scm */
_("61-90 days")
/* src/report/business-reports/gnucash/report/aging.scm */
_("91+ days")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Total")
/* src/report/business-reports/gnucash/report/aging.scm */
_("Total")
/* src/report/business-reports/gnucash/report/aging.scm */
_("No valid account selected.  Click on the Options button and select the account to use.")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Total")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Assets Accounts")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Liability Accounts")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Equity Accounts")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Trading Accounts")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Retained Losses")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Retained Earnings")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Total Equity, Trading, and Liabilities")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("Imbalance Amount")
/* src/report/business-reports/gnucash/report/balsheet-eg.eguile.scm */
_("<strong>Exchange Rates</strong> used for this report")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Balance Sheet (eguile)")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Report Title")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Title for this report")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Balance Sheet Date")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("1- or 2-column report")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("The balance sheet can be displayed with either 1 or 2 columns.  'auto' means that the layout will be adjusted to fit the width of the page.")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Levels of Subaccounts")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Flatten list to depth limit")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Exclude accounts with zero total balances")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Exclude non-top-level accounts with zero balance and no non-zero sub-accounts")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Display accounts as hyperlinks")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Negative amount format")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("The formatting to use for negative amounts: with a leading sign, or enclosing brackets")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Font family")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Font definition in CSS font-family format")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Font size")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Font size in CSS font-size format (e.g. \"medium\" or \"10pt\")")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Template file")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("The file name of the eguile template part of this report.  This file must be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("CSS stylesheet file")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("The file name of the CSS stylesheet to use with this report.  If specified, this file should be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Extra Notes")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Notes added at end of invoice -- may contain HTML markup")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Report's currency")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Price Source")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Show Foreign Currencies")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Display any foreign currency amount in an account")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Commodities")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Notes")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Auto")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Adjust the layout to fit the width of the screen or page")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("One")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Display liabilities and equity below assets")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Two")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Display assets on the left, liabilities and equity on the right")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Sign")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Prefix negative amounts with a minus sign, e.g. -$10.00")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Brackets")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Surround negative amounts with brackets, e.g. ($100.00)")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("(Development version -- don't rely on the numbers on this report without double-checking them.<br>Change the 'Extra Notes' option to get rid of this message)")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Imbalance")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Orphan")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Balance Sheet using eguile-gnc")
/* src/report/business-reports/gnucash/report/balsheet-eg.scm */
_("Display a balance sheet (using eguile template)")
/* src/report/business-reports/gnucash/report/business-reports.scm */
_("_Business")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("From")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("To")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Income Accounts")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Income Accounts")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("The income accounts where the sales and income was recorded.")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Expense Accounts")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Expense Accounts")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("The expense accounts where the expenses are recorded which are subtracted from the sales to give the profit.")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Show Expense Column")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Show the column with the expenses per customer")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Show Company Address")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Show your own company's address and the date of printing")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Date")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Reference")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Type")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Description")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Amount")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Show Lines with All Zeros")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Show the table lines with customers which did not have any transactions in the reporting period, hence would show all zeros in the columns.")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort Column")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Choose the column by which the result table is sorted")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort Order")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Choose the ordering of the column sort: Either ascending or descending")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Balance")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Payment")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Payment")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Unknown")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Total")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Customer Name")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort alphabetically by customer name")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Profit")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort by profit amount")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Markup")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort by markup (which is profit amount divided by sales)")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sales")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort by sales amount")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Expense")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sort by expense amount")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Ascending")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("A to Z, smallest to largest")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Descending")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Z to A, largest to smallest")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Bill")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Expense Report")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Customer")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Vendor")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Employee")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Report")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Customer")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Profit")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Markup")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Sales")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Expense")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("No Customer")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Total")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("%s %s - %s")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("No valid %s selected.  Click on the Options button to select a company.")
/* src/report/business-reports/gnucash/report/customer-summary.scm */
_("Customer Summary")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Invoice Number")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Description")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Charge Type")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Quantity")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Unit Price")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Discount")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Taxable")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Total")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("%")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("T")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Custom Title")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("A custom string to replace Invoice, Bill or Expense Voucher")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the date?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Description")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the description?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Charge Type")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the charge type?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Quantity")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the quantity of items?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Price")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the price per item?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Discount")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the entry's discount")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Taxable")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the entry's taxable status")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display each entry's total total tax")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Total")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the entry's value")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("My Company")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display my company name and address?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("My Company ID")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display my company ID?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display due date?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Individual Taxes")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display all the individual taxes?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Totals")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the totals?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Subtotal")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the subtotals?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("References")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the invoice references?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Billing Terms")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the invoice billing terms?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Billing ID")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the billing id?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Invoice Notes")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the invoice notes?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Payments")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display the payments applied to this invoice?")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Invoice Width")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("The minimum width of the invoice.")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Text")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Extra Notes")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Extra notes to put on the invoice (simple HTML is accepted)")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Thank you for your patronage")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Text")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Today Date Format")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Payment, thank you")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Net Price")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Tax")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Total Price")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Amount Due")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("REF")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Bill")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Expense Voucher")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("%s #%d")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Due")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("INVOICE NOT POSTED")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Billing ID")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Terms")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("No valid invoice selected.  Click on the Options button and select the invoice to use.")
/* src/report/business-reports/gnucash/report/easy-invoice.scm */
_("Easy Invoice")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Invoice Number")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Description")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Charge Type")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Quantity")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Unit Price")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Discount")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Taxable")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Total")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("%")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("T")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Custom Title")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("A custom string to replace Invoice, Bill or Expense Voucher")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the date?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Description")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the description?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Action")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the action?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Quantity")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the quantity of items?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Price")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the price per item?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Discount")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the entry's discount")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Taxable")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the entry's taxable status")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display each entry's total total tax")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Total")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the entry's value")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Individual Taxes")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display all the individual taxes?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Totals")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the totals?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("References")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the invoice references?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Billing Terms")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the invoice billing terms?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Billing ID")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the billing id?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Invoice Notes")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the invoice notes?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Payments")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the payments applied to this invoice?")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Minimum # of entries")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("The minimum number of invoice entries to display. (-1)")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Extra Notes")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Extra notes to put on the invoice")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Thank you for your patronage")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Payable to")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the Payable to: information")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Payable to string")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("The phrase for specifying to whom payments should be made")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Make all cheques Payable to")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Company contact")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display the Company contact information")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Company contact string")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("The phrase used to introduce the company contact")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Direct all inquiries to")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Payment, thank you")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Net Price")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Tax")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Total Price")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Amount Due")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("REF")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Phone:")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Fax:")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Web:")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Bill")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Expense Voucher")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("%s&nbsp;#")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("%s&nbsp;Date")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Invoice in progress...")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Reference")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Terms")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("No valid invoice selected.  Click on the Options button and select the invoice to use.")
/* src/report/business-reports/gnucash/report/fancy-invoice.scm */
_("Fancy Invoice")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Invoice Number")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Description")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Charge Type")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Quantity")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Unit Price")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Discount")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Taxable")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Total")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("%")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("T")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Custom Title")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("A custom string to replace Invoice, Bill or Expense Voucher")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the date?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Description")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the description?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Action")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the action?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Quantity")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the quantity of items?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Price")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the price per item?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Discount")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the entry's discount")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Taxable")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the entry's taxable status")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display each entry's total total tax")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Total")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the entry's value")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Individual Taxes")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display all the individual taxes?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Totals")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the totals?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("References")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the invoice references?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Billing Terms")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the invoice billing terms?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Billing ID")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the billing id?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Invoice Notes")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the invoice notes?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Payments")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display the payments applied to this invoice?")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Extra Notes")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Extra notes to put on the invoice")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Thank you for your patronage")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Today Date Format")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Payment, thank you")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Net Price")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Tax")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Total Price")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Amount Due")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("REF")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Bill")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Expense Voucher")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("%s #%d")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Date")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Invoice in progress...")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Reference")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Terms")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("No valid invoice selected.  Click on the Options button and select the invoice to use.")
/* src/report/business-reports/gnucash/report/invoice.scm */
_("Printable Invoice")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Account")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Job")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Date")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Reference")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Type")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Description")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Amount")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("0-30 days")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("31-60 days")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("61-90 days")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("91+ days")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Balance")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Payment, thank you")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Unknown")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Total Credit")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Total Due")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("The job for this report")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("The account to search for transactions")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("From")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("To")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display the transaction reference?")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display the transaction type?")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display the transaction description?")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Display the transaction amount?")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Today Date Format")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Bill")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Expense Report")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("From")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("To")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Customer")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Job")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Vendor")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Employee")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Report")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Report:")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Report:")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("No valid account selected.  Click on the Options button and select the account to use.")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Today Date Format")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Date Range")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("No valid %s selected.  Click on the Options button to select a company.")
/* src/report/business-reports/gnucash/report/job-report.scm */
_("Job Report")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("From")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("To")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Due or Post Date")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Account")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Date")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Reference")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Type")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Description")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Amount")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Customer")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Employee")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Company")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("No valid customer selected. Click on the Options button to select a customer.")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("No valid employee selected. Click on the Options button to select an employee.")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("No valid company selected. Click on the Options button to select a company.")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Customer")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Employee")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Vendor")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Current")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("0-30 days")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("31-60 days")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("61-90 days")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("91+ days")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Balance")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Payment, thank you")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Unknown")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Total Credit")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Total Due")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("The company for this report")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("The account to search for transactions")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display the transaction reference?")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display the transaction type?")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display the transaction description?")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Display the transaction amount?")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Today Date Format")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Leading date")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Due date is leading")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Post Date")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Post date is leading")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Bill")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Expense Report")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Report")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Report:")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Report:")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("No valid account selected.  Click on the Options button and select the account to use.")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Today Date Format")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Date Range")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Customer Report")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Vendor Report")
/* src/report/business-reports/gnucash/report/owner-report.scm */
_("Employee Report")
/* src/report/business-reports/gnucash/report/payables.scm */
_("Payable Account")
/* src/report/business-reports/gnucash/report/payables.scm */
_("The payable account you wish to examine")
/* src/report/business-reports/gnucash/report/payables.scm */
_("Payable Aging")
/* src/report/business-reports/gnucash/report/receivables.scm */
_("Receivables Account")
/* src/report/business-reports/gnucash/report/receivables.scm */
_("The receivables account you wish to examine")
/* src/report/business-reports/gnucash/report/receivables.scm */
_("Receivable Aging")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Company Name")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Phone")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Fax")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Email")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Website")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Invoice Number")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Invoice in progress...")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Invoice Date")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Due Date")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Date")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Description")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Tax Invoice")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("No invoice has been selected -- please use the Options menu to select one.")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("Tax Invoice")
/* src/report/business-reports/gnucash/report/taxinvoice.eguile.scm */
_("This report is designed for customer (sales) invoices only. Please use the Options menu to select an <em>Invoice</em>, not a Bill or Expense Voucher.")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("n/a")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Headings 1")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Headings 2")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Notes")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Display")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Report title")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Invoice number")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Template file")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("CSS stylesheet file")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Heading font")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Text font")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Logo filename")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Logo width")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Units")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Qty")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Unit Price")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Discount Rate")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Discount Amount")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Net Price")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Tax Rate")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Total Price")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Sub-total")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Amount Due")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Payment received text")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Extra notes")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("The file name of the eguile template part of this report.  This file should either be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("The file name of the CSS stylesheet to use with this report.  This file should either be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Font to use for the main heading")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Font to use for everything else")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Name of a file containing a logo to be used on the report")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Width of the logo in CSS format, e.g. 10% or 32px.  Leave blank to display the logo at its natural width.  The height of the logo will be scaled accordingly.")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Invoice")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Units")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Qty")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Unit Price")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Discount Rate")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Discount Amount")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Net Price")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Tax Rate")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Tax Amount")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Total Price")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Sub-total")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Amount Due")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Payment received, thank you")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Notes added at end of invoice -- may contain HTML markup")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Tax Invoice")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Tax Invoice")
/* src/report/business-reports/gnucash/report/taxinvoice.scm */
_("Display a customer invoice with tax columns (using eguile template)")
/* src/report/business-reports/invoice.scm */
_("Invoice Number")
/* src/report/business-reports/invoice.scm */
_("Date")
/* src/report/business-reports/invoice.scm */
_("Description")
/* src/report/business-reports/invoice.scm */
_("Charge Type")
/* src/report/business-reports/invoice.scm */
_("Quantity")
/* src/report/business-reports/invoice.scm */
_("Unit Price")
/* src/report/business-reports/invoice.scm */
_("Discount")
/* src/report/business-reports/invoice.scm */
_("Taxable")
/* src/report/business-reports/invoice.scm */
_("Tax Amount")
/* src/report/business-reports/invoice.scm */
_("Total")
/* src/report/business-reports/invoice.scm */
_("%")
/* src/report/business-reports/invoice.scm */
_("T")
/* src/report/business-reports/invoice.scm */
_("Custom Title")
/* src/report/business-reports/invoice.scm */
_("A custom string to replace Invoice, Bill or Expense Voucher")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Date")
/* src/report/business-reports/invoice.scm */
_("Display the date?")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Description")
/* src/report/business-reports/invoice.scm */
_("Display the description?")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Action")
/* src/report/business-reports/invoice.scm */
_("Display the action?")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Quantity")
/* src/report/business-reports/invoice.scm */
_("Display the quantity of items?")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Price")
/* src/report/business-reports/invoice.scm */
_("Display the price per item?")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Discount")
/* src/report/business-reports/invoice.scm */
_("Display the entry's discount")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Taxable")
/* src/report/business-reports/invoice.scm */
_("Display the entry's taxable status")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Tax Amount")
/* src/report/business-reports/invoice.scm */
_("Display each entry's total total tax")
/* src/report/business-reports/invoice.scm */
_("Display Columns")
/* src/report/business-reports/invoice.scm */
_("Total")
/* src/report/business-reports/invoice.scm */
_("Display the entry's value")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Individual Taxes")
/* src/report/business-reports/invoice.scm */
_("Display all the individual taxes?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Totals")
/* src/report/business-reports/invoice.scm */
_("Display the totals?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("References")
/* src/report/business-reports/invoice.scm */
_("Display the invoice references?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Billing Terms")
/* src/report/business-reports/invoice.scm */
_("Display the invoice billing terms?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Billing ID")
/* src/report/business-reports/invoice.scm */
_("Display the billing id?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Invoice Notes")
/* src/report/business-reports/invoice.scm */
_("Display the invoice notes?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Payments")
/* src/report/business-reports/invoice.scm */
_("Display the payments applied to this invoice?")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Extra Notes")
/* src/report/business-reports/invoice.scm */
_("Extra notes to put on the invoice")
/* src/report/business-reports/invoice.scm */
_("Thank you for your patronage")
/* src/report/business-reports/invoice.scm */
_("Display")
/* src/report/business-reports/invoice.scm */
_("Today Date Format")
/* src/report/business-reports/invoice.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/invoice.scm */
_("Payment, thank you")
/* src/report/business-reports/invoice.scm */
_("Net Price")
/* src/report/business-reports/invoice.scm */
_("Tax")
/* src/report/business-reports/invoice.scm */
_("Total Price")
/* src/report/business-reports/invoice.scm */
_("Amount Due")
/* src/report/business-reports/invoice.scm */
_("REF")
/* src/report/business-reports/invoice.scm */
_("Invoice")
/* src/report/business-reports/invoice.scm */
_("Bill")
/* src/report/business-reports/invoice.scm */
_("Expense Voucher")
/* src/report/business-reports/invoice.scm */
_("%s #%d")
/* src/report/business-reports/invoice.scm */
_("Date")
/* src/report/business-reports/invoice.scm */
_("Due Date")
/* src/report/business-reports/invoice.scm */
_("Invoice in progress...")
/* src/report/business-reports/invoice.scm */
_("Reference")
/* src/report/business-reports/invoice.scm */
_("Terms")
/* src/report/business-reports/invoice.scm */
_("No valid invoice selected.  Click on the Options button and select the invoice to use.")
/* src/report/business-reports/invoice.scm */
_("Printable Invoice")
/* src/report/business-reports/job-report.scm */
_("Account")
/* src/report/business-reports/job-report.scm */
_("Job")
/* src/report/business-reports/job-report.scm */
_("Date")
/* src/report/business-reports/job-report.scm */
_("Due Date")
/* src/report/business-reports/job-report.scm */
_("Reference")
/* src/report/business-reports/job-report.scm */
_("Type")
/* src/report/business-reports/job-report.scm */
_("Description")
/* src/report/business-reports/job-report.scm */
_("Amount")
/* src/report/business-reports/job-report.scm */
_("0-30 days")
/* src/report/business-reports/job-report.scm */
_("31-60 days")
/* src/report/business-reports/job-report.scm */
_("61-90 days")
/* src/report/business-reports/job-report.scm */
_("91+ days")
/* src/report/business-reports/job-report.scm */
_("Balance")
/* src/report/business-reports/job-report.scm */
_("Payment, thank you")
/* src/report/business-reports/job-report.scm */
_("Unknown")
/* src/report/business-reports/job-report.scm */
_("Total Credit")
/* src/report/business-reports/job-report.scm */
_("Total Due")
/* src/report/business-reports/job-report.scm */
_("The job for this report")
/* src/report/business-reports/job-report.scm */
_("The account to search for transactions")
/* src/report/business-reports/job-report.scm */
_("From")
/* src/report/business-reports/job-report.scm */
_("To")
/* src/report/business-reports/job-report.scm */
_("Display Columns")
/* src/report/business-reports/job-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/job-report.scm */
_("Display Columns")
/* src/report/business-reports/job-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/job-report.scm */
_("Display Columns")
/* src/report/business-reports/job-report.scm */
_("Display the transaction reference?")
/* src/report/business-reports/job-report.scm */
_("Display Columns")
/* src/report/business-reports/job-report.scm */
_("Display the transaction type?")
/* src/report/business-reports/job-report.scm */
_("Display Columns")
/* src/report/business-reports/job-report.scm */
_("Display the transaction description?")
/* src/report/business-reports/job-report.scm */
_("Display Columns")
/* src/report/business-reports/job-report.scm */
_("Display the transaction amount?")
/* src/report/business-reports/job-report.scm */
_("Today Date Format")
/* src/report/business-reports/job-report.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/job-report.scm */
_("Invoice")
/* src/report/business-reports/job-report.scm */
_("Invoice")
/* src/report/business-reports/job-report.scm */
_("Bill")
/* src/report/business-reports/job-report.scm */
_("Expense Report")
/* src/report/business-reports/job-report.scm */
_("From")
/* src/report/business-reports/job-report.scm */
_("To")
/* src/report/business-reports/job-report.scm */
_("Customer")
/* src/report/business-reports/job-report.scm */
_("Job")
/* src/report/business-reports/job-report.scm */
_("Vendor")
/* src/report/business-reports/job-report.scm */
_("Employee")
/* src/report/business-reports/job-report.scm */
_("Report")
/* src/report/business-reports/job-report.scm */
_("Report:")
/* src/report/business-reports/job-report.scm */
_("Report:")
/* src/report/business-reports/job-report.scm */
_("No valid account selected.  Click on the Options button and select the account to use.")
/* src/report/business-reports/job-report.scm */
_("Today Date Format")
/* src/report/business-reports/job-report.scm */
_("Date Range")
/* src/report/business-reports/job-report.scm */
_("No valid %s selected.  Click on the Options button to select a company.")
/* src/report/business-reports/job-report.scm */
_("Job Report")
/* src/report/business-reports/owner-report.scm */
_("From")
/* src/report/business-reports/owner-report.scm */
_("To")
/* src/report/business-reports/owner-report.scm */
_("Due or Post Date")
/* src/report/business-reports/owner-report.scm */
_("Account")
/* src/report/business-reports/owner-report.scm */
_("Date")
/* src/report/business-reports/owner-report.scm */
_("Due Date")
/* src/report/business-reports/owner-report.scm */
_("Reference")
/* src/report/business-reports/owner-report.scm */
_("Type")
/* src/report/business-reports/owner-report.scm */
_("Description")
/* src/report/business-reports/owner-report.scm */
_("Amount")
/* src/report/business-reports/owner-report.scm */
_("Customer")
/* src/report/business-reports/owner-report.scm */
_("Employee")
/* src/report/business-reports/owner-report.scm */
_("Company")
/* src/report/business-reports/owner-report.scm */
_("No valid customer selected. Click on the Options button to select a customer.")
/* src/report/business-reports/owner-report.scm */
_("No valid employee selected. Click on the Options button to select an employee.")
/* src/report/business-reports/owner-report.scm */
_("No valid company selected. Click on the Options button to select a company.")
/* src/report/business-reports/owner-report.scm */
_("Customer")
/* src/report/business-reports/owner-report.scm */
_("Employee")
/* src/report/business-reports/owner-report.scm */
_("Vendor")
/* src/report/business-reports/owner-report.scm */
_("Current")
/* src/report/business-reports/owner-report.scm */
_("0-30 days")
/* src/report/business-reports/owner-report.scm */
_("31-60 days")
/* src/report/business-reports/owner-report.scm */
_("61-90 days")
/* src/report/business-reports/owner-report.scm */
_("91+ days")
/* src/report/business-reports/owner-report.scm */
_("Balance")
/* src/report/business-reports/owner-report.scm */
_("Payment, thank you")
/* src/report/business-reports/owner-report.scm */
_("Unknown")
/* src/report/business-reports/owner-report.scm */
_("Total Credit")
/* src/report/business-reports/owner-report.scm */
_("Total Due")
/* src/report/business-reports/owner-report.scm */
_("The company for this report")
/* src/report/business-reports/owner-report.scm */
_("The account to search for transactions")
/* src/report/business-reports/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/owner-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/owner-report.scm */
_("Display the transaction date?")
/* src/report/business-reports/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/owner-report.scm */
_("Display the transaction reference?")
/* src/report/business-reports/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/owner-report.scm */
_("Display the transaction type?")
/* src/report/business-reports/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/owner-report.scm */
_("Display the transaction description?")
/* src/report/business-reports/owner-report.scm */
_("Display Columns")
/* src/report/business-reports/owner-report.scm */
_("Display the transaction amount?")
/* src/report/business-reports/owner-report.scm */
_("Today Date Format")
/* src/report/business-reports/owner-report.scm */
_("The format for the date->string conversion for today's date.")
/* src/report/business-reports/owner-report.scm */
_("Leading date")
/* src/report/business-reports/owner-report.scm */
_("Due Date")
/* src/report/business-reports/owner-report.scm */
_("Due date is leading")
/* src/report/business-reports/owner-report.scm */
_("Post Date")
/* src/report/business-reports/owner-report.scm */
_("Post date is leading")
/* src/report/business-reports/owner-report.scm */
_("Invoice")
/* src/report/business-reports/owner-report.scm */
_("Bill")
/* src/report/business-reports/owner-report.scm */
_("Expense Report")
/* src/report/business-reports/owner-report.scm */
_("Report")
/* src/report/business-reports/owner-report.scm */
_("Report:")
/* src/report/business-reports/owner-report.scm */
_("Report:")
/* src/report/business-reports/owner-report.scm */
_("No valid account selected.  Click on the Options button and select the account to use.")
/* src/report/business-reports/owner-report.scm */
_("Today Date Format")
/* src/report/business-reports/owner-report.scm */
_("Date Range")
/* src/report/business-reports/owner-report.scm */
_("Customer Report")
/* src/report/business-reports/owner-report.scm */
_("Vendor Report")
/* src/report/business-reports/owner-report.scm */
_("Employee Report")
/* src/report/business-reports/payables.scm */
_("Payable Account")
/* src/report/business-reports/payables.scm */
_("The payable account you wish to examine")
/* src/report/business-reports/payables.scm */
_("Payable Aging")
/* src/report/business-reports/receivables.scm */
_("Receivables Account")
/* src/report/business-reports/receivables.scm */
_("The receivables account you wish to examine")
/* src/report/business-reports/receivables.scm */
_("Receivable Aging")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Invoice")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Company Name")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Phone")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Fax")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Email")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Website")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Invoice Number")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Invoice in progress...")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Invoice Date")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Due Date")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Date")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Description")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Tax Invoice")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("No invoice has been selected -- please use the Options menu to select one.")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("Tax Invoice")
/* src/report/business-reports/taxinvoice.eguile.scm */
_("This report is designed for customer (sales) invoices only. Please use the Options menu to select an <em>Invoice</em>, not a Bill or Expense Voucher.")
/* src/report/business-reports/taxinvoice.scm */
_("n/a")
/* src/report/business-reports/taxinvoice.scm */
_("Headings 1")
/* src/report/business-reports/taxinvoice.scm */
_("Headings 2")
/* src/report/business-reports/taxinvoice.scm */
_("Notes")
/* src/report/business-reports/taxinvoice.scm */
_("Display")
/* src/report/business-reports/taxinvoice.scm */
_("Report title")
/* src/report/business-reports/taxinvoice.scm */
_("Invoice number")
/* src/report/business-reports/taxinvoice.scm */
_("Template file")
/* src/report/business-reports/taxinvoice.scm */
_("CSS stylesheet file")
/* src/report/business-reports/taxinvoice.scm */
_("Heading font")
/* src/report/business-reports/taxinvoice.scm */
_("Text font")
/* src/report/business-reports/taxinvoice.scm */
_("Logo filename")
/* src/report/business-reports/taxinvoice.scm */
_("Logo width")
/* src/report/business-reports/taxinvoice.scm */
_("Units")
/* src/report/business-reports/taxinvoice.scm */
_("Qty")
/* src/report/business-reports/taxinvoice.scm */
_("Unit Price")
/* src/report/business-reports/taxinvoice.scm */
_("Discount Rate")
/* src/report/business-reports/taxinvoice.scm */
_("Discount Amount")
/* src/report/business-reports/taxinvoice.scm */
_("Net Price")
/* src/report/business-reports/taxinvoice.scm */
_("Tax Rate")
/* src/report/business-reports/taxinvoice.scm */
_("Tax Amount")
/* src/report/business-reports/taxinvoice.scm */
_("Total Price")
/* src/report/business-reports/taxinvoice.scm */
_("Sub-total")
/* src/report/business-reports/taxinvoice.scm */
_("Amount Due")
/* src/report/business-reports/taxinvoice.scm */
_("Payment received text")
/* src/report/business-reports/taxinvoice.scm */
_("Extra notes")
/* src/report/business-reports/taxinvoice.scm */
_("The file name of the eguile template part of this report.  This file should either be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/taxinvoice.scm */
_("The file name of the CSS stylesheet to use with this report.  This file should either be in your .gnucash directory, or else in its proper place within the GnuCash installation directories.")
/* src/report/business-reports/taxinvoice.scm */
_("Font to use for the main heading")
/* src/report/business-reports/taxinvoice.scm */
_("Font to use for everything else")
/* src/report/business-reports/taxinvoice.scm */
_("Name of a file containing a logo to be used on the report")
/* src/report/business-reports/taxinvoice.scm */
_("Width of the logo in CSS format, e.g. 10% or 32px.  Leave blank to display the logo at its natural width.  The height of the logo will be scaled accordingly.")
/* src/report/business-reports/taxinvoice.scm */
_("Invoice")
/* src/report/business-reports/taxinvoice.scm */
_("Units")
/* src/report/business-reports/taxinvoice.scm */
_("Qty")
/* src/report/business-reports/taxinvoice.scm */
_("Unit Price")
/* src/report/business-reports/taxinvoice.scm */
_("Discount Rate")
/* src/report/business-reports/taxinvoice.scm */
_("Discount Amount")
/* src/report/business-reports/taxinvoice.scm */
_("Net Price")
/* src/report/business-reports/taxinvoice.scm */
_("Tax Rate")
/* src/report/business-reports/taxinvoice.scm */
_("Tax Amount")
/* src/report/business-reports/taxinvoice.scm */
_("Total Price")
/* src/report/business-reports/taxinvoice.scm */
_("Sub-total")
/* src/report/business-reports/taxinvoice.scm */
_("Amount Due")
/* src/report/business-reports/taxinvoice.scm */
_("Payment received, thank you")
/* src/report/business-reports/taxinvoice.scm */
_("Notes added at end of invoice -- may contain HTML markup")
/* src/report/business-reports/taxinvoice.scm */
_("Tax Invoice")
/* src/report/business-reports/taxinvoice.scm */
_("Tax Invoice")
/* src/report/business-reports/taxinvoice.scm */
_("Display a customer invoice with tax columns (using eguile template)")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Tax Report / TXF Export")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("From")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("To")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Alternate Period")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Override or modify From: & To:")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Use From - To")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Use From - To period")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("1st Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Jan 1 - Mar 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("2nd Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Apr 1 - May 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("3rd Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Jun 1 - Aug 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("4th Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Sep 1 - Dec 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Last Year")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Last Year")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Last Yr 1st Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Jan 1 - Mar 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Last Yr 2nd Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Apr 1 - May 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Last Yr 3rd Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Jun 1 - Aug 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Last Yr 4th Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Sep 1 - Dec 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Select Accounts (none = all)")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Select accounts")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Suppress $0.00 values")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("$0.00 valued Accounts won't be printed.")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Print Full account names")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Print all Parent account names")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("WARNING: There are duplicate TXF codes assigned to some accounts. Only TXF codes with payer sources may be repeated.")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Account Name")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Total")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Period from %s to %s")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Tax Report & XML Export")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Taxable Income / Deductible Expenses / Export to .XML file")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Taxable Income / Deductible Expenses")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("This report shows your Taxable Income and Deductible Expenses.")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("XML")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("Taxable Income / Deductible Expenses")
/* src/report/locale-specific/us/gnucash/report/taxtxf-de_DE.scm */
_("This page shows your Taxable Income and Deductible Expenses.")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Tax Schedule Report/TXF Export")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("From")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("To")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Alternate Period")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Override or modify From: & To:")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Use From - To")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Use From - To period")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("1st Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Jan 1 - Mar 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("2nd Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Apr 1 - May 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("3rd Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Jun 1 - Aug 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("4th Est Tax Quarter")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Sep 1 - Dec 31")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Last Year")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Last Year")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Last Yr 1st Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Jan 1 - Mar 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Last Yr 2nd Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Apr 1 - May 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Last Yr 3rd Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Jun 1 - Aug 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Last Yr 4th Est Tax Qtr")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Sep 1 - Dec 31, Last year")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Select Accounts (none = all)")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Select accounts")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Suppress $0.00 values")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("$0.00 valued Tax codes won't be printed.")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print full account names")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print all Parent account names")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Print all Transfer To/From Accounts")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Print all split details for multi-split transactions")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Print TXF export parameters")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Show TXF export parameters for each TXF code/account on report")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print Action:Memo data")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print Action:Memo data for transactions")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print transaction detail")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print transaction detail for accounts")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not use special date processing")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Do not print transactions out of specified dates")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Currency conversion date")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Select date to use for PriceDB lookups")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Nearest transaction date")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Use nearest to transaction date")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Nearest report date")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Use nearest to report date")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Shade alternate transactions")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Shade background of alternate transactions, if more than one displayed")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Tax Schedule Report & TXF Export")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Taxable Income/Deductible Expenses with Transaction Detail/Export to .TXF file")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Taxable Income/Deductible Expenses")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("This report shows transaction detail for your accounts related to Income Taxes.")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("Taxable Income/Deductible Expenses")
/* src/report/locale-specific/us/gnucash/report/taxtxf.scm */
_("This page shows transaction detail for relevant Income Tax accounts.")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Tax Report / TXF Export")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("From")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("To")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Alternate Period")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Override or modify From: & To:")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Use From - To")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Use From - To period")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("1st Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Jan 1 - Mar 31")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("2nd Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Apr 1 - May 31")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("3rd Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Jun 1 - Aug 31")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("4th Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Sep 1 - Dec 31")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Last Year")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Last Year")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Last Yr 1st Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Jan 1 - Mar 31, Last year")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Last Yr 2nd Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Apr 1 - May 31, Last year")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Last Yr 3rd Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Jun 1 - Aug 31, Last year")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Last Yr 4th Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Sep 1 - Dec 31, Last year")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Select Accounts (none = all)")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Select accounts")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Suppress $0.00 values")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("$0.00 valued Accounts won't be printed.")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Print Full account names")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Print all Parent account names")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("WARNING: There are duplicate TXF codes assigned to some accounts. Only TXF codes with payer sources may be repeated.")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Account Name")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Total")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Period from %s to %s")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Tax Report & XML Export")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Taxable Income / Deductible Expenses / Export to .XML file")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Taxable Income / Deductible Expenses")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("This report shows your Taxable Income and Deductible Expenses.")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("XML")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("Taxable Income / Deductible Expenses")
/* src/report/locale-specific/us/taxtxf-de_DE.scm */
_("This page shows your Taxable Income and Deductible Expenses.")
/* src/report/locale-specific/us/taxtxf.scm */
_("Tax Schedule Report/TXF Export")
/* src/report/locale-specific/us/taxtxf.scm */
_("From")
/* src/report/locale-specific/us/taxtxf.scm */
_("To")
/* src/report/locale-specific/us/taxtxf.scm */
_("Alternate Period")
/* src/report/locale-specific/us/taxtxf.scm */
_("Override or modify From: & To:")
/* src/report/locale-specific/us/taxtxf.scm */
_("Use From - To")
/* src/report/locale-specific/us/taxtxf.scm */
_("Use From - To period")
/* src/report/locale-specific/us/taxtxf.scm */
_("1st Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf.scm */
_("Jan 1 - Mar 31")
/* src/report/locale-specific/us/taxtxf.scm */
_("2nd Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf.scm */
_("Apr 1 - May 31")
/* src/report/locale-specific/us/taxtxf.scm */
_("3rd Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf.scm */
_("Jun 1 - Aug 31")
/* src/report/locale-specific/us/taxtxf.scm */
_("4th Est Tax Quarter")
/* src/report/locale-specific/us/taxtxf.scm */
_("Sep 1 - Dec 31")
/* src/report/locale-specific/us/taxtxf.scm */
_("Last Year")
/* src/report/locale-specific/us/taxtxf.scm */
_("Last Year")
/* src/report/locale-specific/us/taxtxf.scm */
_("Last Yr 1st Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf.scm */
_("Jan 1 - Mar 31, Last year")
/* src/report/locale-specific/us/taxtxf.scm */
_("Last Yr 2nd Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf.scm */
_("Apr 1 - May 31, Last year")
/* src/report/locale-specific/us/taxtxf.scm */
_("Last Yr 3rd Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf.scm */
_("Jun 1 - Aug 31, Last year")
/* src/report/locale-specific/us/taxtxf.scm */
_("Last Yr 4th Est Tax Qtr")
/* src/report/locale-specific/us/taxtxf.scm */
_("Sep 1 - Dec 31, Last year")
/* src/report/locale-specific/us/taxtxf.scm */
_("Select Accounts (none = all)")
/* src/report/locale-specific/us/taxtxf.scm */
_("Select accounts")
/* src/report/locale-specific/us/taxtxf.scm */
_("Suppress $0.00 values")
/* src/report/locale-specific/us/taxtxf.scm */
_("$0.00 valued Tax codes won't be printed.")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print full account names")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print all Parent account names")
/* src/report/locale-specific/us/taxtxf.scm */
_("Print all Transfer To/From Accounts")
/* src/report/locale-specific/us/taxtxf.scm */
_("Print all split details for multi-split transactions")
/* src/report/locale-specific/us/taxtxf.scm */
_("Print TXF export parameters")
/* src/report/locale-specific/us/taxtxf.scm */
_("Show TXF export parameters for each TXF code/account on report")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print Action:Memo data")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print Action:Memo data for transactions")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print transaction detail")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print transaction detail for accounts")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not use special date processing")
/* src/report/locale-specific/us/taxtxf.scm */
_("Do not print transactions out of specified dates")
/* src/report/locale-specific/us/taxtxf.scm */
_("Currency conversion date")
/* src/report/locale-specific/us/taxtxf.scm */
_("Select date to use for PriceDB lookups")
/* src/report/locale-specific/us/taxtxf.scm */
_("Nearest transaction date")
/* src/report/locale-specific/us/taxtxf.scm */
_("Use nearest to transaction date")
/* src/report/locale-specific/us/taxtxf.scm */
_("Nearest report date")
/* src/report/locale-specific/us/taxtxf.scm */
_("Use nearest to report date")
/* src/report/locale-specific/us/taxtxf.scm */
_("Shade alternate transactions")
/* src/report/locale-specific/us/taxtxf.scm */
_("Shade background of alternate transactions, if more than one displayed")
/* src/report/locale-specific/us/taxtxf.scm */
_("Tax Schedule Report & TXF Export")
/* src/report/locale-specific/us/taxtxf.scm */
_("Taxable Income/Deductible Expenses with Transaction Detail/Export to .TXF file")
/* src/report/locale-specific/us/taxtxf.scm */
_("Taxable Income/Deductible Expenses")
/* src/report/locale-specific/us/taxtxf.scm */
_("This report shows transaction detail for your accounts related to Income Taxes.")
/* src/report/locale-specific/us/taxtxf.scm */
_("Taxable Income/Deductible Expenses")
/* src/report/locale-specific/us/taxtxf.scm */
_("This page shows transaction detail for relevant Income Tax accounts.")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("This report has no options.")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("Report")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("Display the %s report")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("Custom Reports")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("Manage and run custom reports")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("Welcome Sample Report")
/* src/report/report-gnome/gnucash/report/report-gnome.scm */
_("Welcome-to-GnuCash report screen")
/* src/report/report-gnome/report-gnome.scm */
_("This report has no options.")
/* src/report/report-gnome/report-gnome.scm */
_("Report")
/* src/report/report-gnome/report-gnome.scm */
_("Display the %s report")
/* src/report/report-gnome/report-gnome.scm */
_("Custom Reports")
/* src/report/report-gnome/report-gnome.scm */
_("Manage and run custom reports")
/* src/report/report-gnome/report-gnome.scm */
_("Welcome Sample Report")
/* src/report/report-gnome/report-gnome.scm */
_("Welcome-to-GnuCash report screen")
/* src/report/report-system/eguile-gnc.scm */
_("An error occurred when processing the template:")
/* src/report/report-system/eguile-gnc.scm */
_("Template file \"%s\" can not be read")
/* src/report/report-system/gnucash/report/eguile-gnc.scm */
_("An error occurred when processing the template:")
/* src/report/report-system/gnucash/report/eguile-gnc.scm */
_("Template file \"%s\" can not be read")
/* src/report/report-system/html-acct-table.scm */
_("Closing Entries")
/* src/report/report-system/html-acct-table.scm */
_("Adjusting Entries")
/* src/report/report-system/html-acct-table.scm */
_("Total")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Title")
/* src/report/report-system/html-fonts.scm */
_("Font info for the report title")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Account link")
/* src/report/report-system/html-fonts.scm */
_("Font info for account name")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Number cell")
/* src/report/report-system/html-fonts.scm */
_("Font info for regular number cells")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Negative Values in Red")
/* src/report/report-system/html-fonts.scm */
_("Display negative values in red.")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Number header")
/* src/report/report-system/html-fonts.scm */
_("Font info for number headers")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Text cell")
/* src/report/report-system/html-fonts.scm */
_("Font info for regular text cells")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Total number cell")
/* src/report/report-system/html-fonts.scm */
_("Font info for number cells containing a total")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Total label cell")
/* src/report/report-system/html-fonts.scm */
_("Font info for cells containing total labels")
/* src/report/report-system/html-fonts.scm */
_("Fonts")
/* src/report/report-system/html-fonts.scm */
_("Centered label cell")
/* src/report/report-system/html-fonts.scm */
_("Font info for centered label cells")
/* src/report/report-system/html-style-sheet.scm */
_("Can't save style sheet")
/* src/report/report-system/html-style-sheet.scm */
_("Default")
/* src/report/report-system/html-utilities.scm */
_("Total")
/* src/report/report-system/html-utilities.scm */
_("Account name")
/* src/report/report-system/html-utilities.scm */
_("Balance")
/* src/report/report-system/html-utilities.scm */
_("Exchange rate")
/* src/report/report-system/html-utilities.scm */
_("Exchange rates")
/* src/report/report-system/html-utilities.scm */
_("No budgets exist.  You must create at least one budget.")
/* src/report/report-system/html-utilities.scm */
_("This report requires you to specify certain report options.")
/* src/report/report-system/html-utilities.scm */
_("Edit report options")
/* src/report/report-system/html-utilities.scm */
_("No accounts selected")
/* src/report/report-system/html-utilities.scm */
_("This report requires accounts to be selected.")
/* src/report/report-system/html-utilities.scm */
_("Edit report options")
/* src/report/report-system/html-utilities.scm */
_("No data")
/* src/report/report-system/html-utilities.scm */
_("The selected accounts contain no data/transactions (or only zeroes) for the selected time period")
/* src/report/report-system/html-utilities.scm */
_("Edit report options")
/* src/report/report-system/options-utilities.scm */
_("Select a date to report on")
/* src/report/report-system/options-utilities.scm */
_("Start of reporting period")
/* src/report/report-system/options-utilities.scm */
_("End of reporting period")
/* src/report/report-system/options-utilities.scm */
_("The amount of time between data points")
/* src/report/report-system/options-utilities.scm */
_("Day")
/* src/report/report-system/options-utilities.scm */
_("Day")
/* src/report/report-system/options-utilities.scm */
_("Week")
/* src/report/report-system/options-utilities.scm */
_("Week")
/* src/report/report-system/options-utilities.scm */
_("2Week")
/* src/report/report-system/options-utilities.scm */
_("Two Weeks")
/* src/report/report-system/options-utilities.scm */
_("Month")
/* src/report/report-system/options-utilities.scm */
_("Month")
/* src/report/report-system/options-utilities.scm */
_("Quarter")
/* src/report/report-system/options-utilities.scm */
_("Quarter")
/* src/report/report-system/options-utilities.scm */
_("Half Year")
/* src/report/report-system/options-utilities.scm */
_("Half Year")
/* src/report/report-system/options-utilities.scm */
_("Year")
/* src/report/report-system/options-utilities.scm */
_("Year")
/* src/report/report-system/options-utilities.scm */
_("All")
/* src/report/report-system/options-utilities.scm */
_("All accounts")
/* src/report/report-system/options-utilities.scm */
_("Top-level")
/* src/report/report-system/options-utilities.scm */
_("Second-level")
/* src/report/report-system/options-utilities.scm */
_("Third-level")
/* src/report/report-system/options-utilities.scm */
_("Fourth-level")
/* src/report/report-system/options-utilities.scm */
_("Fifth-level")
/* src/report/report-system/options-utilities.scm */
_("Sixth-level")
/* src/report/report-system/options-utilities.scm */
_("Show accounts to this depth, overriding any other option.")
/* src/report/report-system/options-utilities.scm */
_("Override account-selection and show sub-accounts of all selected accounts?")
/* src/report/report-system/options-utilities.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/report-system/options-utilities.scm */
_("Include sub-account balances in printed balance?")
/* src/report/report-system/options-utilities.scm */
_("Group the accounts in main categories?")
/* src/report/report-system/options-utilities.scm */
_("Select the currency to display the values of this report in.")
/* src/report/report-system/options-utilities.scm */
_("Display the account's foreign currency amount?")
/* src/report/report-system/options-utilities.scm */
_("The source of price information")
/* src/report/report-system/options-utilities.scm */
_("Average Cost")
/* src/report/report-system/options-utilities.scm */
_("The volume-weighted average cost of purchases")
/* src/report/report-system/options-utilities.scm */
_("Weighted Average")
/* src/report/report-system/options-utilities.scm */
_("The weighted average of all currency transactions of the past")
/* src/report/report-system/options-utilities.scm */
_("Most recent")
/* src/report/report-system/options-utilities.scm */
_("The most recent recorded price")
/* src/report/report-system/options-utilities.scm */
_("Nearest in time")
/* src/report/report-system/options-utilities.scm */
_("The price recorded nearest in time to the report date")
/* src/report/report-system/options-utilities.scm */
_("Width of plot in pixels.")
/* src/report/report-system/options-utilities.scm */
_("Height of plot in pixels.")
/* src/report/report-system/options-utilities.scm */
_("Choose the marker for each data point.")
/* src/report/report-system/options-utilities.scm */
_("Circle")
/* src/report/report-system/options-utilities.scm */
_("Circle")
/* src/report/report-system/options-utilities.scm */
_("Cross")
/* src/report/report-system/options-utilities.scm */
_("Cross")
/* src/report/report-system/options-utilities.scm */
_("Square")
/* src/report/report-system/options-utilities.scm */
_("Square")
/* src/report/report-system/options-utilities.scm */
_("Asterisk")
/* src/report/report-system/options-utilities.scm */
_("Asterisk")
/* src/report/report-system/options-utilities.scm */
_("Filled circle")
/* src/report/report-system/options-utilities.scm */
_("Circle filled with color")
/* src/report/report-system/options-utilities.scm */
_("Filled square")
/* src/report/report-system/options-utilities.scm */
_("Square filled with color")
/* src/report/report-system/options-utilities.scm */
_("Choose the method for sorting accounts.")
/* src/report/report-system/options-utilities.scm */
_("Account Code")
/* src/report/report-system/options-utilities.scm */
_("Alphabetical by account code")
/* src/report/report-system/options-utilities.scm */
_("Alphabetical")
/* src/report/report-system/options-utilities.scm */
_("Alphabetical by account name")
/* src/report/report-system/options-utilities.scm */
_("Amount")
/* src/report/report-system/options-utilities.scm */
_("By amount, largest to smallest")
/* src/report/report-system/options-utilities.scm */
_("How to show the balances of parent accounts")
/* src/report/report-system/options-utilities.scm */
_("Account Balance")
/* src/report/report-system/options-utilities.scm */
_("Show only the balance in the parent account, excluding any subaccounts")
/* src/report/report-system/options-utilities.scm */
_("Subtotal")
/* src/report/report-system/options-utilities.scm */
_("Calculate the subtotal for this parent account and all of its subaccounts, and show this as the parent account balance")
/* src/report/report-system/options-utilities.scm */
_("Do not show")
/* src/report/report-system/options-utilities.scm */
_("Do not show any balances of parent accounts")
/* src/report/report-system/options-utilities.scm */
_("How to show account subtotals for parent accounts")
/* src/report/report-system/options-utilities.scm */
_("Show subtotals")
/* src/report/report-system/options-utilities.scm */
_("Show subtotals for selected parent accounts which have subaccounts")
/* src/report/report-system/options-utilities.scm */
_("Do not show")
/* src/report/report-system/options-utilities.scm */
_("Do not show any subtotals for parent accounts")
/* src/report/report-system/options-utilities.scm */
_("Text book style (experimental)")
/* src/report/report-system/options-utilities.scm */
_("Show parent account subtotals, indented per accounting text book practice (experimental)")
/* src/report/report-system/report.scm */
_("_Assets & Liabilities")
/* src/report/report-system/report.scm */
_("_Income & Expense")
/* src/report/report-system/report.scm */
_("B_udget")
/* src/report/report-system/report.scm */
_("_Taxes")
/* src/report/report-system/report.scm */
_("_Sample & Custom")
/* src/report/report-system/report.scm */
_("_Custom")
/* src/report/report-system/report.scm */
_("General")
/* src/report/report-system/report.scm */
_("Accounts")
/* src/report/report-system/report.scm */
_("Display")
/* src/report/report-system/report.scm */
_("Report name")
/* src/report/report-system/report.scm */
_("Stylesheet")
/* src/report/report-system/report.scm */
_("One of your reports has a report-guid that is a duplicate. Please check the report system, especially your saved reports, for a report with this report-guid: ")
/* src/report/report-system/report.scm */
_("The GnuCash report system has been upgraded. Your old saved reports have been transfered into a new format. If you experience trouble with saved reports, please contact the GnuCash development team.")
/* src/report/report-system/report.scm */
_("Enter a descriptive name for this report")
/* src/report/report-system/report.scm */
_("Select a stylesheet for the report.")
/* src/report/report-system/report.scm */
_("Default")
/* src/report/report-system/report.scm */
_("Stylesheet")
/* src/report/report-system/report.scm */
_("Could not open the file %s. The error is: %s")
/* src/report/report-system/report.scm */
_("Your report \"%s\" has been saved into the configuration file \"%s\".")
/* src/report/report-system/report-utilities.scm */
_("Bank")
/* src/report/report-system/report-utilities.scm */
_("Cash")
/* src/report/report-system/report-utilities.scm */
_("Credits")
/* src/report/report-system/report-utilities.scm */
_("Assets")
/* src/report/report-system/report-utilities.scm */
_("Liabilities")
/* src/report/report-system/report-utilities.scm */
_("Stocks")
/* src/report/report-system/report-utilities.scm */
_("Mutual Funds")
/* src/report/report-system/report-utilities.scm */
_("Currencies")
/* src/report/report-system/report-utilities.scm */
_("Income")
/* src/report/report-system/report-utilities.scm */
_("Expenses")
/* src/report/report-system/report-utilities.scm */
_("Equities")
/* src/report/report-system/report-utilities.scm */
_("Checking")
/* src/report/report-system/report-utilities.scm */
_("Savings")
/* src/report/report-system/report-utilities.scm */
_("Money Market")
/* src/report/report-system/report-utilities.scm */
_("Accounts Receivable")
/* src/report/report-system/report-utilities.scm */
_("Accounts Payable")
/* src/report/report-system/report-utilities.scm */
_("Credit Lines")
/* src/report/report-system/report-utilities.scm */
_("Trading Accounts")
/* src/report/report-system/report-utilities.scm */
_("Building '%s' report ...")
/* src/report/report-system/report-utilities.scm */
_("Rendering '%s' report ...")
/* src/report/standard-reports/account-piecharts.scm */
_("Income Piechart")
/* src/report/standard-reports/account-piecharts.scm */
_("Expense Piechart")
/* src/report/standard-reports/account-piecharts.scm */
_("Asset Piechart")
/* src/report/standard-reports/account-piecharts.scm */
_("Liability Piechart")
/* src/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Income per given time interval")
/* src/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Expenses per given time interval")
/* src/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Assets balance at a given time")
/* src/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Liabilities balance at a given time")
/* src/report/standard-reports/account-piecharts.scm */
_("Income Accounts")
/* src/report/standard-reports/account-piecharts.scm */
_("Expense Accounts")
/* src/report/standard-reports/account-piecharts.scm */
_("Assets")
/* src/report/standard-reports/account-piecharts.scm */
_("Liabilities")
/* src/report/standard-reports/account-piecharts.scm */
_("Start Date")
/* src/report/standard-reports/account-piecharts.scm */
_("End Date")
/* src/report/standard-reports/account-piecharts.scm */
_("Report's currency")
/* src/report/standard-reports/account-piecharts.scm */
_("Price Source")
/* src/report/standard-reports/account-piecharts.scm */
_("Accounts")
/* src/report/standard-reports/account-piecharts.scm */
_("Show Accounts until level")
/* src/report/standard-reports/account-piecharts.scm */
_("Show long account names")
/* src/report/standard-reports/account-piecharts.scm */
_("Show Totals")
/* src/report/standard-reports/account-piecharts.scm */
_("Show Percents")
/* src/report/standard-reports/account-piecharts.scm */
_("Maximum Slices")
/* src/report/standard-reports/account-piecharts.scm */
_("Plot Width")
/* src/report/standard-reports/account-piecharts.scm */
_("Plot Height")
/* src/report/standard-reports/account-piecharts.scm */
_("Sort Method")
/* src/report/standard-reports/account-piecharts.scm */
_("Show Average")
/* src/report/standard-reports/account-piecharts.scm */
_("Select whether the amounts should be shown over the full time period or rather as the average e.g. per month")
/* src/report/standard-reports/account-piecharts.scm */
_("No Averaging")
/* src/report/standard-reports/account-piecharts.scm */
_("Just show the amounts, without any averaging")
/* src/report/standard-reports/account-piecharts.scm */
_("Yearly")
/* src/report/standard-reports/account-piecharts.scm */
_("Show the average yearly amount during the reporting period")
/* src/report/standard-reports/account-piecharts.scm */
_("Monthly")
/* src/report/standard-reports/account-piecharts.scm */
_("Show the average monthly amount during the reporting period")
/* src/report/standard-reports/account-piecharts.scm */
_("Weekly")
/* src/report/standard-reports/account-piecharts.scm */
_("Show the average weekly amount during the reporting period")
/* src/report/standard-reports/account-piecharts.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/account-piecharts.scm */
_("Show accounts to this depth and not further")
/* src/report/standard-reports/account-piecharts.scm */
_("Show the full account name in legend?")
/* src/report/standard-reports/account-piecharts.scm */
_("Show the total balance in legend?")
/* src/report/standard-reports/account-piecharts.scm */
_("Show the percentage in legend?")
/* src/report/standard-reports/account-piecharts.scm */
_("Maximum number of slices in pie")
/* src/report/standard-reports/account-piecharts.scm */
_("Yearly Average")
/* src/report/standard-reports/account-piecharts.scm */
_("Monthly Average")
/* src/report/standard-reports/account-piecharts.scm */
_("Weekly Average")
/* src/report/standard-reports/account-piecharts.scm */
_("Other")
/* src/report/standard-reports/account-piecharts.scm */
_("%s to %s")
/* src/report/standard-reports/account-piecharts.scm */
_("Balance at %s")
/* src/report/standard-reports/account-piecharts.scm */
_("and")
/* src/report/standard-reports/account-summary.scm */
_("Account Summary")
/* src/report/standard-reports/account-summary.scm */
_("Report Title")
/* src/report/standard-reports/account-summary.scm */
_("Title for this report")
/* src/report/standard-reports/account-summary.scm */
_("Company name")
/* src/report/standard-reports/account-summary.scm */
_("Name of company/individual")
/* src/report/standard-reports/account-summary.scm */
_("Date")
/* src/report/standard-reports/account-summary.scm */
_("Accounts")
/* src/report/standard-reports/account-summary.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/account-summary.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/account-summary.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/account-summary.scm */
_("Depth limit behavior")
/* src/report/standard-reports/account-summary.scm */
_("How to treat accounts which exceed the specified depth limit (if any)")
/* src/report/standard-reports/account-summary.scm */
_("Parent account balances")
/* src/report/standard-reports/account-summary.scm */
_("Parent account subtotals")
/* src/report/standard-reports/account-summary.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/account-summary.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/account-summary.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/account-summary.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/account-summary.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/account-summary.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/account-summary.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/account-summary.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/account-summary.scm */
_("Account Balance")
/* src/report/standard-reports/account-summary.scm */
_("Show an account's balance")
/* src/report/standard-reports/account-summary.scm */
_("Account Code")
/* src/report/standard-reports/account-summary.scm */
_("Show an account's account code")
/* src/report/standard-reports/account-summary.scm */
_("Account Type")
/* src/report/standard-reports/account-summary.scm */
_("Show an account's account type")
/* src/report/standard-reports/account-summary.scm */
_("Account Description")
/* src/report/standard-reports/account-summary.scm */
_("Show an account's description")
/* src/report/standard-reports/account-summary.scm */
_("Account Notes")
/* src/report/standard-reports/account-summary.scm */
_("Show an account's notes")
/* src/report/standard-reports/account-summary.scm */
_("Commodities")
/* src/report/standard-reports/account-summary.scm */
_("Report's currency")
/* src/report/standard-reports/account-summary.scm */
_("Price Source")
/* src/report/standard-reports/account-summary.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/account-summary.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/account-summary.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/account-summary.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/account-summary.scm */
_("Recursive Balance")
/* src/report/standard-reports/account-summary.scm */
_("Show the total balance, including balances in subaccounts, of any account at the depth limit")
/* src/report/standard-reports/account-summary.scm */
_("Raise Accounts")
/* src/report/standard-reports/account-summary.scm */
_("Shows accounts deeper than the depth limit at the depth limit")
/* src/report/standard-reports/account-summary.scm */
_("Omit Accounts")
/* src/report/standard-reports/account-summary.scm */
_("Disregard completely any accounts deeper than the depth limit")
/* src/report/standard-reports/account-summary.scm */
_("Code")
/* src/report/standard-reports/account-summary.scm */
_("Type")
/* src/report/standard-reports/account-summary.scm */
_("Description")
/* src/report/standard-reports/account-summary.scm */
_("Account title")
/* src/report/standard-reports/account-summary.scm */
_("Balance")
/* src/report/standard-reports/account-summary.scm */
_("Notes")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Advanced Portfolio")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Price Source")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Share decimal places")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Include accounts with no shares")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Show ticker symbols")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Show listings")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Show prices")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Show number of shares")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Basis calculation method")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Set preference for price list data")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Ignore brokerage fees when calculating returns")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Date")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Report's currency")
/* src/report/standard-reports/advanced-portfolio.scm */
_("The source of price information")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Most recent")
/* src/report/standard-reports/advanced-portfolio.scm */
_("The most recent recorded price")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Nearest in time")
/* src/report/standard-reports/advanced-portfolio.scm */
_("The price recorded nearest in time to the report date")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Most recent to report")
/* src/report/standard-reports/advanced-portfolio.scm */
_("The most recent recorded price before report date")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Basis calculation method")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Average")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Use average cost of all shares for basis")
/* src/report/standard-reports/advanced-portfolio.scm */
_("FIFO")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Use first-in first-out method for basis")
/* src/report/standard-reports/advanced-portfolio.scm */
_("FILO")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Use first-in last-out method for basis")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Prefer use of price editor pricing over transactions, where applicable.")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Ignore brokerage fees when calculating returns")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Display the ticker symbols")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Display exchange listings")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Display numbers of shares in accounts")
/* src/report/standard-reports/advanced-portfolio.scm */
_("The number of decimal places to use for share numbers")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Display share prices")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Accounts")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Stock Accounts to report on")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Include accounts that have a zero share balances.")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Account")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Total")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Symbol")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Listing")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Shares")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Price")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Basis")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Value")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Money In")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Money Out")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Realized Gain")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Unrealized Gain")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Total Gain")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Rate of Gain")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Income")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Brokerage Fees")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Total Return")
/* src/report/standard-reports/advanced-portfolio.scm */
_("Rate of Return")
/* src/report/standard-reports/advanced-portfolio.scm */
_("* this commodity data was built using transaction pricing instead of the price list.")
/* src/report/standard-reports/advanced-portfolio.scm */
_("If you are in a multi-currency situation, the exchanges may not be correct.")
/* src/report/standard-reports/average-balance.scm */
_("Average Balance")
/* src/report/standard-reports/average-balance.scm */
_("Start Date")
/* src/report/standard-reports/average-balance.scm */
_("End Date")
/* src/report/standard-reports/average-balance.scm */
_("Step Size")
/* src/report/standard-reports/average-balance.scm */
_("Report's currency")
/* src/report/standard-reports/average-balance.scm */
_("Price Source")
/* src/report/standard-reports/average-balance.scm */
_("Include Sub-Accounts")
/* src/report/standard-reports/average-balance.scm */
_("Exclude transactions between selected accounts")
/* src/report/standard-reports/average-balance.scm */
_("Include sub-accounts of all selected accounts")
/* src/report/standard-reports/average-balance.scm */
_("Exclude transactions that only involve two accounts, both of which are selected below.  This only affects the profit and loss columns of the table.")
/* src/report/standard-reports/average-balance.scm */
_("Accounts")
/* src/report/standard-reports/average-balance.scm */
_("Do transaction report on this account")
/* src/report/standard-reports/average-balance.scm */
_("Show table")
/* src/report/standard-reports/average-balance.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/average-balance.scm */
_("Show plot")
/* src/report/standard-reports/average-balance.scm */
_("Display a graph of the selected data.")
/* src/report/standard-reports/average-balance.scm */
_("Plot Type")
/* src/report/standard-reports/average-balance.scm */
_("The type of graph to generate")
/* src/report/standard-reports/average-balance.scm */
_("Average")
/* src/report/standard-reports/average-balance.scm */
_("Average Balance")
/* src/report/standard-reports/average-balance.scm */
_("Profit")
/* src/report/standard-reports/average-balance.scm */
_("Profit (Gain minus Loss)")
/* src/report/standard-reports/average-balance.scm */
_("Gain/Loss")
/* src/report/standard-reports/average-balance.scm */
_("Gain And Loss")
/* src/report/standard-reports/average-balance.scm */
_("Plot Width")
/* src/report/standard-reports/average-balance.scm */
_("Plot Height")
/* src/report/standard-reports/average-balance.scm */
_("Period start")
/* src/report/standard-reports/average-balance.scm */
_("Period end")
/* src/report/standard-reports/average-balance.scm */
_("Average")
/* src/report/standard-reports/average-balance.scm */
_("Maximum")
/* src/report/standard-reports/average-balance.scm */
_("Minimum")
/* src/report/standard-reports/average-balance.scm */
_("Gain")
/* src/report/standard-reports/average-balance.scm */
_("Loss")
/* src/report/standard-reports/average-balance.scm */
_("Profit")
/* src/report/standard-reports/average-balance.scm */
_("Accounts")
/* src/report/standard-reports/average-balance.scm */
_("Plot Type")
/* src/report/standard-reports/average-balance.scm */
_("Show plot")
/* src/report/standard-reports/average-balance.scm */
_("Show table")
/* src/report/standard-reports/average-balance.scm */
_("Plot Width")
/* src/report/standard-reports/average-balance.scm */
_("Plot Height")
/* src/report/standard-reports/balance-sheet.scm */
_("Balance Sheet")
/* src/report/standard-reports/balance-sheet.scm */
_("Report Title")
/* src/report/standard-reports/balance-sheet.scm */
_("Title for this report")
/* src/report/standard-reports/balance-sheet.scm */
_("Company name")
/* src/report/standard-reports/balance-sheet.scm */
_("Name of company/individual")
/* src/report/standard-reports/balance-sheet.scm */
_("Balance Sheet Date")
/* src/report/standard-reports/balance-sheet.scm */
_("Single column Balance Sheet")
/* src/report/standard-reports/balance-sheet.scm */
_("Print liability/equity section in the same column under the assets section as opposed to a second column right of the assets section")
/* src/report/standard-reports/balance-sheet.scm */
_("Accounts")
/* src/report/standard-reports/balance-sheet.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/balance-sheet.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/balance-sheet.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/balance-sheet.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/balance-sheet.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/balance-sheet.scm */
_("Parent account balances")
/* src/report/standard-reports/balance-sheet.scm */
_("Parent account subtotals")
/* src/report/standard-reports/balance-sheet.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/balance-sheet.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/balance-sheet.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/balance-sheet.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/balance-sheet.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/balance-sheet.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/balance-sheet.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/balance-sheet.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/balance-sheet.scm */
_("Label the assets section")
/* src/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a label for the assets section")
/* src/report/standard-reports/balance-sheet.scm */
_("Include assets total")
/* src/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a line indicating total assets")
/* src/report/standard-reports/balance-sheet.scm */
_("Label the liabilities section")
/* src/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a label for the liabilities section")
/* src/report/standard-reports/balance-sheet.scm */
_("Include liabilities total")
/* src/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a line indicating total liabilities")
/* src/report/standard-reports/balance-sheet.scm */
_("Label the equity section")
/* src/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a label for the equity section")
/* src/report/standard-reports/balance-sheet.scm */
_("Include equity total")
/* src/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a line indicating total equity")
/* src/report/standard-reports/balance-sheet.scm */
_("Commodities")
/* src/report/standard-reports/balance-sheet.scm */
_("Report's currency")
/* src/report/standard-reports/balance-sheet.scm */
_("Price Source")
/* src/report/standard-reports/balance-sheet.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/balance-sheet.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/balance-sheet.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/balance-sheet.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/balance-sheet.scm */
_("Assets")
/* src/report/standard-reports/balance-sheet.scm */
_("Total Assets")
/* src/report/standard-reports/balance-sheet.scm */
_("Liabilities")
/* src/report/standard-reports/balance-sheet.scm */
_("Total Liabilities")
/* src/report/standard-reports/balance-sheet.scm */
_("Equity")
/* src/report/standard-reports/balance-sheet.scm */
_("Retained Earnings")
/* src/report/standard-reports/balance-sheet.scm */
_("Retained Losses")
/* src/report/standard-reports/balance-sheet.scm */
_("Trading Gains")
/* src/report/standard-reports/balance-sheet.scm */
_("Trading Losses")
/* src/report/standard-reports/balance-sheet.scm */
_("Unrealized Gains")
/* src/report/standard-reports/balance-sheet.scm */
_("Unrealized Losses")
/* src/report/standard-reports/balance-sheet.scm */
_("Total Equity")
/* src/report/standard-reports/balance-sheet.scm */
_("Total Liabilities & Equity")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Budget Balance Sheet")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Report Title")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Title for this report")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Company name")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Name of company/individual")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Single column Balance Sheet")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Print liability/equity section in the same column under the assets section as opposed to a second column right of the assets section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Accounts")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Parent account balances")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Parent account subtotals")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Label the assets section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a label for the assets section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Include assets total")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a line indicating total assets")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Label the liabilities section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a label for the liabilities section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Include liabilities total")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a line indicating total liabilities")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Label the equity section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a label for the equity section")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Include equity total")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a line indicating total equity")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Include new/existing totals")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include lines indicating change in totals introduced by budget")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Commodities")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Report's currency")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Price Source")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Budget")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Budget to use.")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Assets")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Existing Assets")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Allocated Assets")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Unallocated Assets")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Total Assets")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Liabilities")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Existing Liabilities")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("New Liabilities")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Total Liabilities")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Equity")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Existing Retained Earnings")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Existing Retained Losses")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("New Retained Earnings")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("New Retained Losses")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Total Retained Earnings")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Total Retained Losses")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Unrealized Gains")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Unrealized Losses")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Existing Equity")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("New Equity")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Total Equity")
/* src/report/standard-reports/budget-balance-sheet.scm */
_("Total Liabilities & Equity")
/* src/report/standard-reports/budget-barchart.scm */
_("Budget Barchart")
/* src/report/standard-reports/budget-barchart.scm */
_("Accounts")
/* src/report/standard-reports/budget-barchart.scm */
_("Budget")
/* src/report/standard-reports/budget-barchart.scm */
_("Running Sum")
/* src/report/standard-reports/budget-barchart.scm */
_("Budget")
/* src/report/standard-reports/budget-barchart.scm */
_("Calculate as running sum?")
/* src/report/standard-reports/budget-barchart.scm */
_("Report on these accounts")
/* src/report/standard-reports/budget-barchart.scm */
_("Budget")
/* src/report/standard-reports/budget-barchart.scm */
_("Actual")
/* src/report/standard-reports/budget-barchart.scm */
_("Budget Barchart")
/* src/report/standard-reports/budget-flow.scm */
_("Budget Flow")
/* src/report/standard-reports/budget-flow.scm */
_("Account")
/* src/report/standard-reports/budget-flow.scm */
_("Price Source")
/* src/report/standard-reports/budget-flow.scm */
_("Budget")
/* src/report/standard-reports/budget-flow.scm */
_("Period")
/* src/report/standard-reports/budget-flow.scm */
_("Report's currency")
/* src/report/standard-reports/budget-flow.scm */
_("Budget")
/* src/report/standard-reports/budget-flow.scm */
_("Period")
/* src/report/standard-reports/budget-flow.scm */
_("Report on these accounts")
/* src/report/standard-reports/budget-flow.scm */
_("Total")
/* src/report/standard-reports/budget-flow.scm */
_("Total")
/* src/report/standard-reports/budget-flow.scm */
_("%s: %s - %s")
/* src/report/standard-reports/budget-income-statement.scm */
_("Report Title")
/* src/report/standard-reports/budget-income-statement.scm */
_("Title for this report")
/* src/report/standard-reports/budget-income-statement.scm */
_("Company name")
/* src/report/standard-reports/budget-income-statement.scm */
_("Name of company/individual")
/* src/report/standard-reports/budget-income-statement.scm */
_("Budget")
/* src/report/standard-reports/budget-income-statement.scm */
_("Budget to use.")
/* src/report/standard-reports/budget-income-statement.scm */
_("Report for range of budget periods")
/* src/report/standard-reports/budget-income-statement.scm */
_("Create report for a budget period range instead of the entire budget.")
/* src/report/standard-reports/budget-income-statement.scm */
_("Range start")
/* src/report/standard-reports/budget-income-statement.scm */
_("Select a budget period that begins the reporting range.")
/* src/report/standard-reports/budget-income-statement.scm */
_("Range end")
/* src/report/standard-reports/budget-income-statement.scm */
_("Select a budget period that ends the reporting range.")
/* src/report/standard-reports/budget-income-statement.scm */
_("Accounts")
/* src/report/standard-reports/budget-income-statement.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/budget-income-statement.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/budget-income-statement.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/budget-income-statement.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/budget-income-statement.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/budget-income-statement.scm */
_("Parent account balances")
/* src/report/standard-reports/budget-income-statement.scm */
_("Parent account subtotals")
/* src/report/standard-reports/budget-income-statement.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/budget-income-statement.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/budget-income-statement.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/budget-income-statement.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/budget-income-statement.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/budget-income-statement.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/budget-income-statement.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/budget-income-statement.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/budget-income-statement.scm */
_("Label the revenue section")
/* src/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a label for the revenue section")
/* src/report/standard-reports/budget-income-statement.scm */
_("Include revenue total")
/* src/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a line indicating total revenue")
/* src/report/standard-reports/budget-income-statement.scm */
_("Label the expense section")
/* src/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a label for the expense section")
/* src/report/standard-reports/budget-income-statement.scm */
_("Include expense total")
/* src/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a line indicating total expense")
/* src/report/standard-reports/budget-income-statement.scm */
_("Commodities")
/* src/report/standard-reports/budget-income-statement.scm */
_("Report's currency")
/* src/report/standard-reports/budget-income-statement.scm */
_("Price Source")
/* src/report/standard-reports/budget-income-statement.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/budget-income-statement.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/budget-income-statement.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/budget-income-statement.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/budget-income-statement.scm */
_("Entries")
/* src/report/standard-reports/budget-income-statement.scm */
_("Display as a two column report")
/* src/report/standard-reports/budget-income-statement.scm */
_("Divides the report into an income column and an expense column")
/* src/report/standard-reports/budget-income-statement.scm */
_("Display in standard, income first, order")
/* src/report/standard-reports/budget-income-statement.scm */
_("Causes the report to display in the standard order, placing income before expenses")
/* src/report/standard-reports/budget-income-statement.scm */
_("Reporting range end period cannot be less than start period.")
/* src/report/standard-reports/budget-income-statement.scm */
_("for Budget %s Period %u")
/* src/report/standard-reports/budget-income-statement.scm */
_("for Budget %s Periods %u - %u")
/* src/report/standard-reports/budget-income-statement.scm */
_("for Budget %s")
/* src/report/standard-reports/budget-income-statement.scm */
_("Revenues")
/* src/report/standard-reports/budget-income-statement.scm */
_("Total Revenue")
/* src/report/standard-reports/budget-income-statement.scm */
_("Expenses")
/* src/report/standard-reports/budget-income-statement.scm */
_("Total Expenses")
/* src/report/standard-reports/budget-income-statement.scm */
_("Net income")
/* src/report/standard-reports/budget-income-statement.scm */
_("Net loss")
/* src/report/standard-reports/budget-income-statement.scm */
_("Budget Income Statement")
/* src/report/standard-reports/budget-income-statement.scm */
_("Budget Profit & Loss")
/* src/report/standard-reports/budget.scm */
_("Budget Report")
/* src/report/standard-reports/budget.scm */
_("Account Display Depth")
/* src/report/standard-reports/budget.scm */
_("Always show sub-accounts")
/* src/report/standard-reports/budget.scm */
_("Account")
/* src/report/standard-reports/budget.scm */
_("Price Source")
/* src/report/standard-reports/budget.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/budget.scm */
_("Show Full Account Names")
/* src/report/standard-reports/budget.scm */
_("Select Columns")
/* src/report/standard-reports/budget.scm */
_("Show Budget")
/* src/report/standard-reports/budget.scm */
_("Display a column for the budget values")
/* src/report/standard-reports/budget.scm */
_("Show Actual")
/* src/report/standard-reports/budget.scm */
_("Display a column for the actual values")
/* src/report/standard-reports/budget.scm */
_("Show Difference")
/* src/report/standard-reports/budget.scm */
_("Display the difference as budget - actual")
/* src/report/standard-reports/budget.scm */
_("Show Column with Totals")
/* src/report/standard-reports/budget.scm */
_("Display a column with the row totals")
/* src/report/standard-reports/budget.scm */
_("Roll up budget amounts to parent")
/* src/report/standard-reports/budget.scm */
_("If parent account does not have its own budget value, use the sum of the child account budget values")
/* src/report/standard-reports/budget.scm */
_("Include accounts with zero total balances and budget values")
/* src/report/standard-reports/budget.scm */
_("Include accounts with zero total (recursive) balances and budget values in this report")
/* src/report/standard-reports/budget.scm */
_("Compress prior/later periods")
/* src/report/standard-reports/budget.scm */
_("Accumulate columns for periods before and after the current period to allow focus on the current period.")
/* src/report/standard-reports/budget.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/budget.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/budget.scm */
_("Budget")
/* src/report/standard-reports/budget.scm */
_("Budget")
/* src/report/standard-reports/budget.scm */
_("Show full account names (including parent accounts)")
/* src/report/standard-reports/budget.scm */
_("Bgt")
/* src/report/standard-reports/budget.scm */
_("Act")
/* src/report/standard-reports/budget.scm */
_("Diff")
/* src/report/standard-reports/budget.scm */
_("%s: %s")
/* src/report/standard-reports/cash-flow.scm */
_("Cash Flow")
/* src/report/standard-reports/cash-flow.scm */
_("Start Date")
/* src/report/standard-reports/cash-flow.scm */
_("End Date")
/* src/report/standard-reports/cash-flow.scm */
_("Account Display Depth")
/* src/report/standard-reports/cash-flow.scm */
_("Always show sub-accounts")
/* src/report/standard-reports/cash-flow.scm */
_("Account")
/* src/report/standard-reports/cash-flow.scm */
_("Report's currency")
/* src/report/standard-reports/cash-flow.scm */
_("Price Source")
/* src/report/standard-reports/cash-flow.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/cash-flow.scm */
_("Show Full Account Names")
/* src/report/standard-reports/cash-flow.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/cash-flow.scm */
_("Show full account names (including parent accounts)")
/* src/report/standard-reports/cash-flow.scm */
_("%s to %s")
/* src/report/standard-reports/cash-flow.scm */
_("%s and subaccounts")
/* src/report/standard-reports/cash-flow.scm */
_("%s and selected subaccounts")
/* src/report/standard-reports/cash-flow.scm */
_("Selected Accounts")
/* src/report/standard-reports/cash-flow.scm */
_("Money into selected accounts comes from")
/* src/report/standard-reports/cash-flow.scm */
_("Money In")
/* src/report/standard-reports/cash-flow.scm */
_("Money out of selected accounts goes to")
/* src/report/standard-reports/cash-flow.scm */
_("Money Out")
/* src/report/standard-reports/cash-flow.scm */
_("Difference")
/* src/report/standard-reports/category-barchart.scm */
_("Income Barchart")
/* src/report/standard-reports/category-barchart.scm */
_("Expense Barchart")
/* src/report/standard-reports/category-barchart.scm */
_("Asset Barchart")
/* src/report/standard-reports/category-barchart.scm */
_("Liability Barchart")
/* src/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Income per interval developing over time")
/* src/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Expenses per interval developing over time")
/* src/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Assets developing over time")
/* src/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Liabilities developing over time")
/* src/report/standard-reports/category-barchart.scm */
_("Income Over Time")
/* src/report/standard-reports/category-barchart.scm */
_("Expense Over Time")
/* src/report/standard-reports/category-barchart.scm */
_("Assets Over Time")
/* src/report/standard-reports/category-barchart.scm */
_("Liabilities Over Time")
/* src/report/standard-reports/category-barchart.scm */
_("Start Date")
/* src/report/standard-reports/category-barchart.scm */
_("End Date")
/* src/report/standard-reports/category-barchart.scm */
_("Step Size")
/* src/report/standard-reports/category-barchart.scm */
_("Report's currency")
/* src/report/standard-reports/category-barchart.scm */
_("Price Source")
/* src/report/standard-reports/category-barchart.scm */
_("Accounts")
/* src/report/standard-reports/category-barchart.scm */
_("Show Accounts until level")
/* src/report/standard-reports/category-barchart.scm */
_("Show long account names")
/* src/report/standard-reports/category-barchart.scm */
_("Use Stacked Bars")
/* src/report/standard-reports/category-barchart.scm */
_("Maximum Bars")
/* src/report/standard-reports/category-barchart.scm */
_("Plot Width")
/* src/report/standard-reports/category-barchart.scm */
_("Plot Height")
/* src/report/standard-reports/category-barchart.scm */
_("Sort Method")
/* src/report/standard-reports/category-barchart.scm */
_("Show Average")
/* src/report/standard-reports/category-barchart.scm */
_("Select whether the amounts should be shown over the full time period or rather as the average e.g. per month")
/* src/report/standard-reports/category-barchart.scm */
_("No Averaging")
/* src/report/standard-reports/category-barchart.scm */
_("Just show the amounts, without any averaging")
/* src/report/standard-reports/category-barchart.scm */
_("Monthly")
/* src/report/standard-reports/category-barchart.scm */
_("Show the average monthly amount during the reporting period")
/* src/report/standard-reports/category-barchart.scm */
_("Weekly")
/* src/report/standard-reports/category-barchart.scm */
_("Show the average weekly amount during the reporting period")
/* src/report/standard-reports/category-barchart.scm */
_("Daily")
/* src/report/standard-reports/category-barchart.scm */
_("Show the average daily amount during the reporting period")
/* src/report/standard-reports/category-barchart.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/category-barchart.scm */
_("Show accounts to this depth and not further")
/* src/report/standard-reports/category-barchart.scm */
_("Show the full account name in legend?")
/* src/report/standard-reports/category-barchart.scm */
_("Show barchart as stacked barchart?")
/* src/report/standard-reports/category-barchart.scm */
_("Maximum number of bars in the chart")
/* src/report/standard-reports/category-barchart.scm */
_("Show table")
/* src/report/standard-reports/category-barchart.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/category-barchart.scm */
_("Show table")
/* src/report/standard-reports/category-barchart.scm */
_("Monthly Average")
/* src/report/standard-reports/category-barchart.scm */
_("Weekly Average")
/* src/report/standard-reports/category-barchart.scm */
_("Daily Average")
/* src/report/standard-reports/category-barchart.scm */
_("%s to %s")
/* src/report/standard-reports/category-barchart.scm */
_("Balances %s to %s")
/* src/report/standard-reports/category-barchart.scm */
_("Other")
/* src/report/standard-reports/category-barchart.scm */
_("and")
/* src/report/standard-reports/category-barchart.scm */
_("Date")
/* src/report/standard-reports/category-barchart.scm */
_("and")
/* src/report/standard-reports/category-barchart.scm */
_("Grand Total")
/* src/report/standard-reports/daily-reports.scm */
_("Income vs. Day of Week")
/* src/report/standard-reports/daily-reports.scm */
_("Expenses vs. Day of Week")
/* src/report/standard-reports/daily-reports.scm */
_("Shows a piechart with the total income for each day of the week")
/* src/report/standard-reports/daily-reports.scm */
_("Shows a piechart with the total expenses for each day of the week")
/* src/report/standard-reports/daily-reports.scm */
_("Income vs. Day of Week")
/* src/report/standard-reports/daily-reports.scm */
_("Expenses vs. Day of Week")
/* src/report/standard-reports/daily-reports.scm */
_("Start Date")
/* src/report/standard-reports/daily-reports.scm */
_("End Date")
/* src/report/standard-reports/daily-reports.scm */
_("Report's currency")
/* src/report/standard-reports/daily-reports.scm */
_("Price Source")
/* src/report/standard-reports/daily-reports.scm */
_("Accounts")
/* src/report/standard-reports/daily-reports.scm */
_("Show Accounts until level")
/* src/report/standard-reports/daily-reports.scm */
_("Include Sub-Accounts")
/* src/report/standard-reports/daily-reports.scm */
_("Show long account names")
/* src/report/standard-reports/daily-reports.scm */
_("Show Totals")
/* src/report/standard-reports/daily-reports.scm */
_("Maximum Slices")
/* src/report/standard-reports/daily-reports.scm */
_("Plot Width")
/* src/report/standard-reports/daily-reports.scm */
_("Plot Height")
/* src/report/standard-reports/daily-reports.scm */
_("Sort Method")
/* src/report/standard-reports/daily-reports.scm */
_("Include sub-accounts of all selected accounts")
/* src/report/standard-reports/daily-reports.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/daily-reports.scm */
_("Show accounts to this depth and not further")
/* src/report/standard-reports/daily-reports.scm */
_("Show the total balance in legend?")
/* src/report/standard-reports/daily-reports.scm */
_("Sunday")
/* src/report/standard-reports/daily-reports.scm */
_("Monday")
/* src/report/standard-reports/daily-reports.scm */
_("Tuesday")
/* src/report/standard-reports/daily-reports.scm */
_("Wednesday")
/* src/report/standard-reports/daily-reports.scm */
_("Thursday")
/* src/report/standard-reports/daily-reports.scm */
_("Friday")
/* src/report/standard-reports/daily-reports.scm */
_("Saturday")
/* src/report/standard-reports/daily-reports.scm */
_("%s to %s")
/* src/report/standard-reports/equity-statement.scm */
_("Equity Statement")
/* src/report/standard-reports/equity-statement.scm */
_("Report Title")
/* src/report/standard-reports/equity-statement.scm */
_("Title for this report")
/* src/report/standard-reports/equity-statement.scm */
_("Company name")
/* src/report/standard-reports/equity-statement.scm */
_("Name of company/individual")
/* src/report/standard-reports/equity-statement.scm */
_("Start Date")
/* src/report/standard-reports/equity-statement.scm */
_("End Date")
/* src/report/standard-reports/equity-statement.scm */
_("Accounts")
/* src/report/standard-reports/equity-statement.scm */
_("Report only on these accounts")
/* src/report/standard-reports/equity-statement.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/equity-statement.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/equity-statement.scm */
_("Commodities")
/* src/report/standard-reports/equity-statement.scm */
_("Report's currency")
/* src/report/standard-reports/equity-statement.scm */
_("Price Source")
/* src/report/standard-reports/equity-statement.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/equity-statement.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/equity-statement.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/equity-statement.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/equity-statement.scm */
_("Entries")
/* src/report/standard-reports/equity-statement.scm */
_("Closing Entries pattern")
/* src/report/standard-reports/equity-statement.scm */
_("Any text in the Description column which identifies closing entries")
/* src/report/standard-reports/equity-statement.scm */
_("Closing Entries pattern is case-sensitive")
/* src/report/standard-reports/equity-statement.scm */
_("Causes the Closing Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/equity-statement.scm */
_("Closing Entries Pattern is regular expression")
/* src/report/standard-reports/equity-statement.scm */
_("Causes the Closing Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/equity-statement.scm */
_("General")
/* src/report/standard-reports/equity-statement.scm */
_("General")
/* src/report/standard-reports/equity-statement.scm */
_("Closing Entries")
/* src/report/standard-reports/equity-statement.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/equity-statement.scm */
_("for Period")
/* src/report/standard-reports/equity-statement.scm */
_("%s to %s")
/* src/report/standard-reports/equity-statement.scm */
_("Capital")
/* src/report/standard-reports/equity-statement.scm */
_("Net income")
/* src/report/standard-reports/equity-statement.scm */
_("Net loss")
/* src/report/standard-reports/equity-statement.scm */
_("Investments")
/* src/report/standard-reports/equity-statement.scm */
_("Withdrawals")
/* src/report/standard-reports/equity-statement.scm */
_("Unrealized Gains")
/* src/report/standard-reports/equity-statement.scm */
_("Unrealized Losses")
/* src/report/standard-reports/equity-statement.scm */
_("Increase in capital")
/* src/report/standard-reports/equity-statement.scm */
_("Decrease in capital")
/* src/report/standard-reports/equity-statement.scm */
_("Capital")
/* src/report/standard-reports/general-journal.scm */
_("General Journal")
/* src/report/standard-reports/general-journal.scm */
_("Register")
/* src/report/standard-reports/general-journal.scm */
_("Debit")
/* src/report/standard-reports/general-journal.scm */
_("Credit")
/* src/report/standard-reports/general-journal.scm */
_("Title")
/* src/report/standard-reports/general-journal.scm */
_("Date")
/* src/report/standard-reports/general-journal.scm */
_("Num")
/* src/report/standard-reports/general-journal.scm */
_("Description")
/* src/report/standard-reports/general-journal.scm */
_("Account")
/* src/report/standard-reports/general-journal.scm */
_("Shares")
/* src/report/standard-reports/general-journal.scm */
_("Price")
/* src/report/standard-reports/general-journal.scm */
_("Amount")
/* src/report/standard-reports/general-journal.scm */
_("Running Balance")
/* src/report/standard-reports/general-journal.scm */
_("Totals")
/* src/report/standard-reports/general-ledger.scm */
_("General Ledger")
/* src/report/standard-reports/general-ledger.scm */
_("Sorting")
/* src/report/standard-reports/general-ledger.scm */
_("Filter Type")
/* src/report/standard-reports/general-ledger.scm */
_("Void Transactions")
/* src/report/standard-reports/general-ledger.scm */
_("Date")
/* src/report/standard-reports/general-ledger.scm */
_("Reconciled Date")
/* src/report/standard-reports/general-ledger.scm */
_("Num")
/* src/report/standard-reports/general-ledger.scm */
_("Description")
/* src/report/standard-reports/general-ledger.scm */
_("Memo")
/* src/report/standard-reports/general-ledger.scm */
_("Account Name")
/* src/report/standard-reports/general-ledger.scm */
_("Use Full Account Name")
/* src/report/standard-reports/general-ledger.scm */
_("Account Code")
/* src/report/standard-reports/general-ledger.scm */
_("Other Account Name")
/* src/report/standard-reports/general-ledger.scm */
_("Use Full Other Account Name")
/* src/report/standard-reports/general-ledger.scm */
_("Other Account Code")
/* src/report/standard-reports/general-ledger.scm */
_("Shares")
/* src/report/standard-reports/general-ledger.scm */
_("Price")
/* src/report/standard-reports/general-ledger.scm */
_("Amount")
/* src/report/standard-reports/general-ledger.scm */
_("Running Balance")
/* src/report/standard-reports/general-ledger.scm */
_("Totals")
/* src/report/standard-reports/general-ledger.scm */
_("Sign Reverses")
/* src/report/standard-reports/general-ledger.scm */
_("Style")
/* src/report/standard-reports/general-ledger.scm */
_("Primary Key")
/* src/report/standard-reports/general-ledger.scm */
_("Show Full Account Name")
/* src/report/standard-reports/general-ledger.scm */
_("Show Account Code")
/* src/report/standard-reports/general-ledger.scm */
_("Primary Subtotal")
/* src/report/standard-reports/general-ledger.scm */
_("Primary Subtotal for Date Key")
/* src/report/standard-reports/general-ledger.scm */
_("Primary Sort Order")
/* src/report/standard-reports/general-ledger.scm */
_("Secondary Key")
/* src/report/standard-reports/general-ledger.scm */
_("Secondary Subtotal")
/* src/report/standard-reports/general-ledger.scm */
_("Secondary Subtotal for Date Key")
/* src/report/standard-reports/general-ledger.scm */
_("Secondary Sort Order")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Income Piechart")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Expense Piechart")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Asset Piechart")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Liability Piechart")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Income per given time interval")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Expenses per given time interval")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Assets balance at a given time")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Shows a piechart with the Liabilities balance at a given time")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Income Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Expense Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show Accounts until level")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show long account names")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show Percents")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Maximum Slices")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Sort Method")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show Average")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Select whether the amounts should be shown over the full time period or rather as the average e.g. per month")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("No Averaging")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Just show the amounts, without any averaging")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Yearly")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show the average yearly amount during the reporting period")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Monthly")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show the average monthly amount during the reporting period")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Weekly")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show the average weekly amount during the reporting period")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show accounts to this depth and not further")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show the full account name in legend?")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show the total balance in legend?")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Show the percentage in legend?")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Maximum number of slices in pie")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Yearly Average")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Monthly Average")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Weekly Average")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Other")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("Balance at %s")
/* src/report/standard-reports/gnucash/report/standard-reports/account-piecharts.scm */
_("and")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account Summary")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Depth limit behavior")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("How to treat accounts which exceed the specified depth limit (if any)")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Parent account balances")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Parent account subtotals")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show an account's balance")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show an account's account code")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account Type")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show an account's account type")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account Description")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show an account's description")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show an account's notes")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Recursive Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Show the total balance, including balances in subaccounts, of any account at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Raise Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Shows accounts deeper than the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Omit Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Disregard completely any accounts deeper than the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Code")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Type")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Account title")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/account-summary.scm */
_("Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Advanced Portfolio")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Share decimal places")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Include accounts with no shares")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Show ticker symbols")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Show listings")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Show prices")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Show number of shares")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Basis calculation method")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Set preference for price list data")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Ignore brokerage fees when calculating returns")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("The source of price information")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Most recent")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("The most recent recorded price")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Nearest in time")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("The price recorded nearest in time to the report date")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Most recent to report")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("The most recent recorded price before report date")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Basis calculation method")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Average")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Use average cost of all shares for basis")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("FIFO")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Use first-in first-out method for basis")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("FILO")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Use first-in last-out method for basis")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Prefer use of price editor pricing over transactions, where applicable.")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Ignore brokerage fees when calculating returns")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Display the ticker symbols")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Display exchange listings")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Display numbers of shares in accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("The number of decimal places to use for share numbers")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Display share prices")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Stock Accounts to report on")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Include accounts that have a zero share balances.")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Total")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Symbol")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Listing")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Basis")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Value")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Money In")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Money Out")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Realized Gain")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Unrealized Gain")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Total Gain")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Rate of Gain")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Income")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Brokerage Fees")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Total Return")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("Rate of Return")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("* this commodity data was built using transaction pricing instead of the price list.")
/* src/report/standard-reports/gnucash/report/standard-reports/advanced-portfolio.scm */
_("If you are in a multi-currency situation, the exchanges may not be correct.")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Average Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Step Size")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Include Sub-Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Exclude transactions between selected accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Include sub-accounts of all selected accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Exclude transactions that only involve two accounts, both of which are selected below.  This only affects the profit and loss columns of the table.")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Do transaction report on this account")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Show plot")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Display a graph of the selected data.")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Plot Type")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("The type of graph to generate")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Average")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Average Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Profit (Gain minus Loss)")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Gain/Loss")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Gain And Loss")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Period start")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Period end")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Average")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Maximum")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Minimum")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Gain")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Loss")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Plot Type")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Show plot")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/average-balance.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Balance Sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Balance Sheet Date")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Single column Balance Sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Print liability/equity section in the same column under the assets section as opposed to a second column right of the assets section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Parent account balances")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Parent account subtotals")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Label the assets section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a label for the assets section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Include assets total")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a line indicating total assets")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Label the liabilities section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a label for the liabilities section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Include liabilities total")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a line indicating total liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Label the equity section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a label for the equity section")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Include equity total")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Whether or not to include a line indicating total equity")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Total Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Total Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Retained Earnings")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Retained Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Trading Gains")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Trading Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Unrealized Gains")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Unrealized Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Total Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/balance-sheet.scm */
_("Total Liabilities & Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Budget Balance Sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Single column Balance Sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Print liability/equity section in the same column under the assets section as opposed to a second column right of the assets section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Parent account balances")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Parent account subtotals")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Label the assets section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a label for the assets section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Include assets total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a line indicating total assets")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Label the liabilities section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a label for the liabilities section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Include liabilities total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a line indicating total liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Label the equity section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a label for the equity section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Include equity total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include a line indicating total equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Include new/existing totals")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Whether or not to include lines indicating change in totals introduced by budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Budget to use.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Existing Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Allocated Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Unallocated Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Total Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Existing Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("New Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Total Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Existing Retained Earnings")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Existing Retained Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("New Retained Earnings")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("New Retained Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Total Retained Earnings")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Total Retained Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Unrealized Gains")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Unrealized Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Existing Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("New Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Total Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-balance-sheet.scm */
_("Total Liabilities & Equity")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Budget Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Running Sum")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Calculate as running sum?")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Report on these accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Actual")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-barchart.scm */
_("Budget Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Budget Flow")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Period")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Period")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Report on these accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("Total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-flow.scm */
_("%s: %s - %s")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Budget to use.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Report for range of budget periods")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Create report for a budget period range instead of the entire budget.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Range start")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Select a budget period that begins the reporting range.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Range end")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Select a budget period that ends the reporting range.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Parent account balances")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Parent account subtotals")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Label the revenue section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a label for the revenue section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Include revenue total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a line indicating total revenue")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Label the expense section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a label for the expense section")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Include expense total")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Whether or not to include a line indicating total expense")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Display as a two column report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Divides the report into an income column and an expense column")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Display in standard, income first, order")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Causes the report to display in the standard order, placing income before expenses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Reporting range end period cannot be less than start period.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("for Budget %s Period %u")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("for Budget %s Periods %u - %u")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("for Budget %s")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Revenues")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Total Revenue")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Expenses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Total Expenses")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Net income")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Net loss")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Budget Income Statement")
/* src/report/standard-reports/gnucash/report/standard-reports/budget-income-statement.scm */
_("Budget Profit & Loss")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Budget Report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Account Display Depth")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Always show sub-accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show Full Account Names")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Select Columns")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Display a column for the budget values")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show Actual")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Display a column for the actual values")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show Difference")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Display the difference as budget - actual")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show Column with Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Display a column with the row totals")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Roll up budget amounts to parent")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("If parent account does not have its own budget value, use the sum of the child account budget values")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Include accounts with zero total balances and budget values")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Include accounts with zero total (recursive) balances and budget values in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Compress prior/later periods")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Accumulate columns for periods before and after the current period to allow focus on the current period.")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Budget")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Show full account names (including parent accounts)")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Bgt")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Act")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("Diff")
/* src/report/standard-reports/gnucash/report/standard-reports/budget.scm */
_("%s: %s")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Cash Flow")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Account Display Depth")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Always show sub-accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Show Full Account Names")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Show full account names (including parent accounts)")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("%s and subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("%s and selected subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Selected Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Money into selected accounts comes from")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Money In")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Money out of selected accounts goes to")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Money Out")
/* src/report/standard-reports/gnucash/report/standard-reports/cash-flow.scm */
_("Difference")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Income Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Expense Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Asset Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Liability Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Income per interval developing over time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Expenses per interval developing over time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Assets developing over time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Shows a barchart with the Liabilities developing over time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Income Over Time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Expense Over Time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Assets Over Time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Liabilities Over Time")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Step Size")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show Accounts until level")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show long account names")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Use Stacked Bars")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Maximum Bars")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Sort Method")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show Average")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Select whether the amounts should be shown over the full time period or rather as the average e.g. per month")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("No Averaging")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Just show the amounts, without any averaging")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Monthly")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show the average monthly amount during the reporting period")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Weekly")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show the average weekly amount during the reporting period")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Daily")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show the average daily amount during the reporting period")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show accounts to this depth and not further")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show the full account name in legend?")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show barchart as stacked barchart?")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Maximum number of bars in the chart")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Monthly Average")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Weekly Average")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Daily Average")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Balances %s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Other")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("and")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("and")
/* src/report/standard-reports/gnucash/report/standard-reports/category-barchart.scm */
_("Grand Total")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Income vs. Day of Week")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Expenses vs. Day of Week")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Shows a piechart with the total income for each day of the week")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Shows a piechart with the total expenses for each day of the week")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Income vs. Day of Week")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Expenses vs. Day of Week")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Show Accounts until level")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Include Sub-Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Show long account names")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Show Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Maximum Slices")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Sort Method")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Include sub-accounts of all selected accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Show accounts to this depth and not further")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Show the total balance in legend?")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Sunday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Monday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Tuesday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Wednesday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Thursday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Friday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("Saturday")
/* src/report/standard-reports/gnucash/report/standard-reports/daily-reports.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Equity Statement")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Report only on these accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Closing Entries pattern")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Any text in the Description column which identifies closing entries")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Closing Entries pattern is case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Causes the Closing Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Closing Entries Pattern is regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Causes the Closing Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("General")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("General")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Closing Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("for Period")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Capital")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Net income")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Net loss")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Investments")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Withdrawals")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Unrealized Gains")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Unrealized Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Increase in capital")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Decrease in capital")
/* src/report/standard-reports/gnucash/report/standard-reports/equity-statement.scm */
_("Capital")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("General Journal")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Register")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Debit")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Credit")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Title")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Running Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/general-journal.scm */
_("Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("General Ledger")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Sorting")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Filter Type")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Void Transactions")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Reconciled Date")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Use Full Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Use Full Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Other Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Running Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Sign Reverses")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Style")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Primary Key")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Show Full Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Show Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Primary Subtotal")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Primary Subtotal for Date Key")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Primary Sort Order")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Secondary Key")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Secondary Subtotal")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Secondary Subtotal for Date Key")
/* src/report/standard-reports/gnucash/report/standard-reports/general-ledger.scm */
_("Secondary Sort Order")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Parent account balances")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Parent account subtotals")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Label the revenue section")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Whether or not to include a label for the revenue section")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Include revenue total")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Whether or not to include a line indicating total revenue")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Label the trading accounts section")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Whether or not to include a label for the trading accounts section")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Include trading accounts total")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Whether or not to include a line indicating total trading accounts balance")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Label the expense section")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Whether or not to include a label for the expense section")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Include expense total")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Whether or not to include a line indicating total expense")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Closing Entries pattern")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Any text in the Description column which identifies closing entries")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Closing Entries pattern is case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Causes the Closing Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Closing Entries Pattern is regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Causes the Closing Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Display as a two column report")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Divides the report into an income column and an expense column")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Display in standard, income first, order")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Causes the report to display in the standard order, placing income before expenses")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Closing Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("for Period")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Revenues")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Total Revenue")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Expenses")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Total Expenses")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Trading")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Total Trading")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Net income")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Net loss")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Income Statement")
/* src/report/standard-reports/gnucash/report/standard-reports/income-statement.scm */
_("Profit & Loss")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Income/Expense Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Step Size")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show Income/Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show Asset & Liability bars")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show Net Worth bars")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show Income and Expenses?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show the Asset and the Liability bars?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show the net profit?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show a Net Worth bar?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Income")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Net Worth")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Income Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Asset Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Expense Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Liability Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Income")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Net Worth")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Net Worth Barchart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-barchart.scm */
_("Income & Expense Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Income/Expense Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Step Size")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show Income/Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show Asset & Liability")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show Net Worth")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Line Width")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Set line width in pixels")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Data markers?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Y Grid")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show Income and Expenses?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show the Asset and the Liability bars?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show the net profit?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show a Net Worth bar?")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Add horizontal grid lines.")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Display a mark for each data point.")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Show table")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Income")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Net Worth")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Income Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Asset Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Expense Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Liability Chart")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Income")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Assets")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Liabilities")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Net Worth")
/* src/report/standard-reports/gnucash/report/standard-reports/net-linechart.scm */
_("Net Worth Linechart")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Investment Portfolio")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Share decimal places")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Include accounts with no shares")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("The number of decimal places to use for share numbers")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Stock Accounts to report on")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Include accounts that have a zero share balances.")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Symbol")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Listing")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Units")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Value")
/* src/report/standard-reports/gnucash/report/standard-reports/portfolio.scm */
_("Total")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Step Size")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Price of Commodity")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Invert prices")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Show Income/Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Show Net Profit")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Show Asset & Liability bars")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Show Net Worth bars")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Marker")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Marker Color")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Plot Width")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Plot Height")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Calculate the price of this commodity.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("The source of price information")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Weighted Average")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("The weighted average of all currency transactions of the past")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Actual Transactions")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("The instantaneous price of actual currency transactions in the past")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Price Database")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("The recorded prices")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Plot commodity per currency rather than currency per commodity.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Color of the marker")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Days")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Weeks")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Double-Weeks")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Months")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Years")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("All Prices equal")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("All the prices found are equal. This would result in a plot with one straight line. Unfortunately, the plotting tool can't handle that.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("All Prices at the same date")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("All the prices found are from the same date. This would result in a plot with one straight line. Unfortunately, the plotting tool can't handle that.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Only one price")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("There was only one single price found for the selected commodities in the selected time period. This doesn't give a useful plot.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("No data")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("There is no price information available for the selected commodities in the selected time period.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Identical commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Your selected commodity and the currency of the report are identical. It doesn't make sense to show prices for identical commodities.")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/price-scatter.scm */
_("Price Scatterplot")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Transfer")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Lot")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Value")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Debit Value")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Credit Value")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("-- Split Transaction --")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Debit")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Credit")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("General")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Title")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("The title of the report")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Register Report")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the date?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the check number?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the description?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the memo?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the account?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the number of shares?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Lot")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the name of lot the shares are in?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the shares price?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the amount?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Single")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Single Column Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Double")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Two Column Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Value")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the value in transaction currency?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Running Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display a running balance")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Display the totals?")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Total Debits")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Total Credits")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Total Value Debits")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Total Value Credits")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Net Change")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Value Change")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Client")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Invoice")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Charge")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/register.scm */
_("Register")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Future Scheduled Transactions Summary")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Depth limit behavior")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("How to treat accounts which exceed the specified depth limit (if any)")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Parent account balances")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Parent account subtotals")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Account Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show an account's balance")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show an account's account code")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Account Type")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show an account's account type")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Account Description")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show an account's description")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Account Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show an account's notes")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Recursive Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Show the total balance, including balances in subaccounts, of any account at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Raise Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Shows accounts deeper than the depth limit at the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Omit Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Disregard completely any accounts deeper than the depth limit")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Code")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Type")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Account title")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/sx-summary.scm */
_("Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Transaction Report")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sorting")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Primary Key")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Primary Subtotal")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Primary Subtotal for Date Key")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Secondary Key")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Secondary Subtotal")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Secondary Subtotal for Date Key")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Void Transactions")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Table for Exporting")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Common Currency")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Split Transaction")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Total For ")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Grand Total")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Running Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Use Full Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Other Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Use Full Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sorting")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sorting")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show Full Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Transfer from/to")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Debit")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Credit")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Start Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("End Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Style")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Report style")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Multi-Line")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display N lines")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Single")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display 1 line")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Convert all transactions into a common currency")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Formats the table suitable for cut & paste exporting with extra cells")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Report on these accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Filter By...")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Filter on these accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Filter Type")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Filter account")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Do not do any filtering")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Include Transactions to/from Filter Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Include transactions to/from filter accounts only")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Exclude Transactions to/from Filter Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Exclude transactions to/from all filter accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("How to handle void transactions")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Non-void only")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show only non-voided transactions")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Void only")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show only voided transactions")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Both")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show both (and include void transactions in totals)")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Do not sort")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort & subtotal by account name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort & subtotal by account code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Exact Time")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by exact time")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by the Reconciled Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Register Order")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort as with the register")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by account transferred from/to's name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Other Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by account transferred from/to's code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by amount")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by description")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Number")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by check/transaction number")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by memo")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Ascending")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("smallest to largest, earliest to latest")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Descending")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("largest to smallest, latest to earliest")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Weekly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Weekly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Monthly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Monthly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Quarterly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Quarterly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Yearly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Yearly")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by this criterion first")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show Full Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show the full account name for subtotals and subtitles?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Show the account code for subtotals and subtitles?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Subtotal according to the primary key?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Do a date subtotal")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Primary Sort Order")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Order of primary sorting")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sort by this criterion second")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Subtotal according to the secondary key?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Do a date subtotal")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Secondary Sort Order")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Order of Secondary sorting")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the date?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the reconciled date?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Num")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the check number?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the description?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the notes if the memo is unavailable?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the account name?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Use Full Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the full account name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the account code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the other account name? (if this is a split transaction, this parameter is guessed).")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Use Full Other Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the full account name")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Other Account Code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the other account code")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Shares")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the number of shares?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Price")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the shares price?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Running Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display a running balance")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Totals")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the totals?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the memo?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display the amount?")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("No amount display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Single")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Single Column Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Double")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Two Column Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sign Reverses")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reverse amount display for certain account types")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Don't change any displayed amounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Income and Expense")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reverse amount display for Income and Expense Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Credit Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Reverse amount display for Liability, Payable, Equity, Credit Card, and Income accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("From %s To %s")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Primary Subtotals/headings")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Secondary Subtotals/headings")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Grand Total")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Split Odd")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Split Even")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Sign Reverses")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("Style")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("-- Split Transaction --")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("No matching transactions found")
/* src/report/standard-reports/gnucash/report/standard-reports/transaction.scm */
_("No transactions were found that match the time interval and account selection specified in the Options panel.")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Trial Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Report Title")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Title for this report")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Company name")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Name of company/individual")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Start of Adjusting/Closing")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Date of Report")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Report variation")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Kind of trial balance to generate")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Report on these accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Merchandising")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Gross adjustment accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Do not net, but show gross debit/credit adjustments to these accounts. Merchandising businesses will normally select their inventory accounts here.")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Income summary accounts")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjustments made to these accounts are gross adjusted (see above) in the Adjustments, Adjusted Trial Balance, and Income Statement columns. Mostly useful for merchandising businesses.")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjusting Entries pattern")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Any text in the Description column which identifies adjusting entries")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjusting Entries pattern is case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Causes the Adjusting Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjusting Entries Pattern is regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Causes the Adjusting Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Closing Entries pattern")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Any text in the Description column which identifies closing entries")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Closing Entries pattern is case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Causes the Closing Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Closing Entries Pattern is regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Causes the Closing Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Commodities")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Report's currency")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Price Source")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("General")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("General")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Current Trial Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Uses the exact balances in the general ledger")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Pre-adjustment Trial Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Ignores Adjusting/Closing entries")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Work Sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Creates a complete end-of-period work sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjusting Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Closing Entries")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("for Period")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("%s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Trial Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjustments")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Adjusted Trial Balance")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Income Statement")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Balance Sheet")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Debit")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Credit")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Account Name")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Unrealized Gains")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Unrealized Losses")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Net Income")
/* src/report/standard-reports/gnucash/report/standard-reports/trial-balance.scm */
_("Net Loss")
/* src/report/standard-reports/income-statement.scm */
_("Report Title")
/* src/report/standard-reports/income-statement.scm */
_("Title for this report")
/* src/report/standard-reports/income-statement.scm */
_("Company name")
/* src/report/standard-reports/income-statement.scm */
_("Name of company/individual")
/* src/report/standard-reports/income-statement.scm */
_("Start Date")
/* src/report/standard-reports/income-statement.scm */
_("End Date")
/* src/report/standard-reports/income-statement.scm */
_("Accounts")
/* src/report/standard-reports/income-statement.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/income-statement.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/income-statement.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/income-statement.scm */
_("Flatten list to depth limit")
/* src/report/standard-reports/income-statement.scm */
_("Displays accounts which exceed the depth limit at the depth limit")
/* src/report/standard-reports/income-statement.scm */
_("Parent account balances")
/* src/report/standard-reports/income-statement.scm */
_("Parent account subtotals")
/* src/report/standard-reports/income-statement.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/income-statement.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/income-statement.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/income-statement.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/income-statement.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/income-statement.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/income-statement.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/income-statement.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/income-statement.scm */
_("Label the revenue section")
/* src/report/standard-reports/income-statement.scm */
_("Whether or not to include a label for the revenue section")
/* src/report/standard-reports/income-statement.scm */
_("Include revenue total")
/* src/report/standard-reports/income-statement.scm */
_("Whether or not to include a line indicating total revenue")
/* src/report/standard-reports/income-statement.scm */
_("Label the trading accounts section")
/* src/report/standard-reports/income-statement.scm */
_("Whether or not to include a label for the trading accounts section")
/* src/report/standard-reports/income-statement.scm */
_("Include trading accounts total")
/* src/report/standard-reports/income-statement.scm */
_("Whether or not to include a line indicating total trading accounts balance")
/* src/report/standard-reports/income-statement.scm */
_("Label the expense section")
/* src/report/standard-reports/income-statement.scm */
_("Whether or not to include a label for the expense section")
/* src/report/standard-reports/income-statement.scm */
_("Include expense total")
/* src/report/standard-reports/income-statement.scm */
_("Whether or not to include a line indicating total expense")
/* src/report/standard-reports/income-statement.scm */
_("Commodities")
/* src/report/standard-reports/income-statement.scm */
_("Report's currency")
/* src/report/standard-reports/income-statement.scm */
_("Price Source")
/* src/report/standard-reports/income-statement.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/income-statement.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/income-statement.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/income-statement.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/income-statement.scm */
_("Entries")
/* src/report/standard-reports/income-statement.scm */
_("Closing Entries pattern")
/* src/report/standard-reports/income-statement.scm */
_("Any text in the Description column which identifies closing entries")
/* src/report/standard-reports/income-statement.scm */
_("Closing Entries pattern is case-sensitive")
/* src/report/standard-reports/income-statement.scm */
_("Causes the Closing Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/income-statement.scm */
_("Closing Entries Pattern is regular expression")
/* src/report/standard-reports/income-statement.scm */
_("Causes the Closing Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/income-statement.scm */
_("Display as a two column report")
/* src/report/standard-reports/income-statement.scm */
_("Divides the report into an income column and an expense column")
/* src/report/standard-reports/income-statement.scm */
_("Display in standard, income first, order")
/* src/report/standard-reports/income-statement.scm */
_("Causes the report to display in the standard order, placing income before expenses")
/* src/report/standard-reports/income-statement.scm */
_("Closing Entries")
/* src/report/standard-reports/income-statement.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/income-statement.scm */
_("for Period")
/* src/report/standard-reports/income-statement.scm */
_("%s to %s")
/* src/report/standard-reports/income-statement.scm */
_("Revenues")
/* src/report/standard-reports/income-statement.scm */
_("Total Revenue")
/* src/report/standard-reports/income-statement.scm */
_("Expenses")
/* src/report/standard-reports/income-statement.scm */
_("Total Expenses")
/* src/report/standard-reports/income-statement.scm */
_("Trading")
/* src/report/standard-reports/income-statement.scm */
_("Total Trading")
/* src/report/standard-reports/income-statement.scm */
_("Net income")
/* src/report/standard-reports/income-statement.scm */
_("Net loss")
/* src/report/standard-reports/income-statement.scm */
_("Income Statement")
/* src/report/standard-reports/income-statement.scm */
_("Profit & Loss")
/* src/report/standard-reports/net-barchart.scm */
_("Income/Expense Chart")
/* src/report/standard-reports/net-barchart.scm */
_("Start Date")
/* src/report/standard-reports/net-barchart.scm */
_("End Date")
/* src/report/standard-reports/net-barchart.scm */
_("Step Size")
/* src/report/standard-reports/net-barchart.scm */
_("Report's currency")
/* src/report/standard-reports/net-barchart.scm */
_("Price Source")
/* src/report/standard-reports/net-barchart.scm */
_("Accounts")
/* src/report/standard-reports/net-barchart.scm */
_("Show Income/Expense")
/* src/report/standard-reports/net-barchart.scm */
_("Show Net Profit")
/* src/report/standard-reports/net-barchart.scm */
_("Show Asset & Liability bars")
/* src/report/standard-reports/net-barchart.scm */
_("Show Net Worth bars")
/* src/report/standard-reports/net-barchart.scm */
_("Plot Width")
/* src/report/standard-reports/net-barchart.scm */
_("Plot Height")
/* src/report/standard-reports/net-barchart.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/net-barchart.scm */
_("Show Income and Expenses?")
/* src/report/standard-reports/net-barchart.scm */
_("Show the Asset and the Liability bars?")
/* src/report/standard-reports/net-barchart.scm */
_("Show the net profit?")
/* src/report/standard-reports/net-barchart.scm */
_("Show a Net Worth bar?")
/* src/report/standard-reports/net-barchart.scm */
_("Show table")
/* src/report/standard-reports/net-barchart.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/net-barchart.scm */
_("Show table")
/* src/report/standard-reports/net-barchart.scm */
_("%s to %s")
/* src/report/standard-reports/net-barchart.scm */
_("Income")
/* src/report/standard-reports/net-barchart.scm */
_("Expense")
/* src/report/standard-reports/net-barchart.scm */
_("Assets")
/* src/report/standard-reports/net-barchart.scm */
_("Liabilities")
/* src/report/standard-reports/net-barchart.scm */
_("Net Profit")
/* src/report/standard-reports/net-barchart.scm */
_("Net Worth")
/* src/report/standard-reports/net-barchart.scm */
_("Income Chart")
/* src/report/standard-reports/net-barchart.scm */
_("Asset Chart")
/* src/report/standard-reports/net-barchart.scm */
_("Expense Chart")
/* src/report/standard-reports/net-barchart.scm */
_("Liability Chart")
/* src/report/standard-reports/net-barchart.scm */
_("Date")
/* src/report/standard-reports/net-barchart.scm */
_("Income")
/* src/report/standard-reports/net-barchart.scm */
_("Expense")
/* src/report/standard-reports/net-barchart.scm */
_("Assets")
/* src/report/standard-reports/net-barchart.scm */
_("Liabilities")
/* src/report/standard-reports/net-barchart.scm */
_("Net Profit")
/* src/report/standard-reports/net-barchart.scm */
_("Net Worth")
/* src/report/standard-reports/net-barchart.scm */
_("Net Worth Barchart")
/* src/report/standard-reports/net-barchart.scm */
_("Income & Expense Chart")
/* src/report/standard-reports/net-linechart.scm */
_("Income/Expense Chart")
/* src/report/standard-reports/net-linechart.scm */
_("Start Date")
/* src/report/standard-reports/net-linechart.scm */
_("End Date")
/* src/report/standard-reports/net-linechart.scm */
_("Step Size")
/* src/report/standard-reports/net-linechart.scm */
_("Report's currency")
/* src/report/standard-reports/net-linechart.scm */
_("Price Source")
/* src/report/standard-reports/net-linechart.scm */
_("Accounts")
/* src/report/standard-reports/net-linechart.scm */
_("Show Income/Expense")
/* src/report/standard-reports/net-linechart.scm */
_("Show Net Profit")
/* src/report/standard-reports/net-linechart.scm */
_("Show Asset & Liability")
/* src/report/standard-reports/net-linechart.scm */
_("Show Net Worth")
/* src/report/standard-reports/net-linechart.scm */
_("Plot Width")
/* src/report/standard-reports/net-linechart.scm */
_("Plot Height")
/* src/report/standard-reports/net-linechart.scm */
_("Line Width")
/* src/report/standard-reports/net-linechart.scm */
_("Set line width in pixels")
/* src/report/standard-reports/net-linechart.scm */
_("Data markers?")
/* src/report/standard-reports/net-linechart.scm */
_("Y Grid")
/* src/report/standard-reports/net-linechart.scm */
_("Report on these accounts, if chosen account level allows.")
/* src/report/standard-reports/net-linechart.scm */
_("Show Income and Expenses?")
/* src/report/standard-reports/net-linechart.scm */
_("Show the Asset and the Liability bars?")
/* src/report/standard-reports/net-linechart.scm */
_("Show the net profit?")
/* src/report/standard-reports/net-linechart.scm */
_("Show a Net Worth bar?")
/* src/report/standard-reports/net-linechart.scm */
_("Show table")
/* src/report/standard-reports/net-linechart.scm */
_("Display a table of the selected data.")
/* src/report/standard-reports/net-linechart.scm */
_("Add horizontal grid lines.")
/* src/report/standard-reports/net-linechart.scm */
_("Display a mark for each data point.")
/* src/report/standard-reports/net-linechart.scm */
_("Show table")
/* src/report/standard-reports/net-linechart.scm */
_("%s to %s")
/* src/report/standard-reports/net-linechart.scm */
_("Income")
/* src/report/standard-reports/net-linechart.scm */
_("Expense")
/* src/report/standard-reports/net-linechart.scm */
_("Assets")
/* src/report/standard-reports/net-linechart.scm */
_("Liabilities")
/* src/report/standard-reports/net-linechart.scm */
_("Net Profit")
/* src/report/standard-reports/net-linechart.scm */
_("Net Worth")
/* src/report/standard-reports/net-linechart.scm */
_("Income Chart")
/* src/report/standard-reports/net-linechart.scm */
_("Asset Chart")
/* src/report/standard-reports/net-linechart.scm */
_("Expense Chart")
/* src/report/standard-reports/net-linechart.scm */
_("Liability Chart")
/* src/report/standard-reports/net-linechart.scm */
_("Date")
/* src/report/standard-reports/net-linechart.scm */
_("Income")
/* src/report/standard-reports/net-linechart.scm */
_("Expense")
/* src/report/standard-reports/net-linechart.scm */
_("Assets")
/* src/report/standard-reports/net-linechart.scm */
_("Liabilities")
/* src/report/standard-reports/net-linechart.scm */
_("Net Profit")
/* src/report/standard-reports/net-linechart.scm */
_("Net Worth")
/* src/report/standard-reports/net-linechart.scm */
_("Net Worth Linechart")
/* src/report/standard-reports/portfolio.scm */
_("Investment Portfolio")
/* src/report/standard-reports/portfolio.scm */
_("Price Source")
/* src/report/standard-reports/portfolio.scm */
_("Share decimal places")
/* src/report/standard-reports/portfolio.scm */
_("Include accounts with no shares")
/* src/report/standard-reports/portfolio.scm */
_("Date")
/* src/report/standard-reports/portfolio.scm */
_("Report's currency")
/* src/report/standard-reports/portfolio.scm */
_("The number of decimal places to use for share numbers")
/* src/report/standard-reports/portfolio.scm */
_("Accounts")
/* src/report/standard-reports/portfolio.scm */
_("Stock Accounts to report on")
/* src/report/standard-reports/portfolio.scm */
_("Include accounts that have a zero share balances.")
/* src/report/standard-reports/portfolio.scm */
_("Account")
/* src/report/standard-reports/portfolio.scm */
_("Symbol")
/* src/report/standard-reports/portfolio.scm */
_("Listing")
/* src/report/standard-reports/portfolio.scm */
_("Units")
/* src/report/standard-reports/portfolio.scm */
_("Price")
/* src/report/standard-reports/portfolio.scm */
_("Value")
/* src/report/standard-reports/portfolio.scm */
_("Total")
/* src/report/standard-reports/price-scatter.scm */
_("Start Date")
/* src/report/standard-reports/price-scatter.scm */
_("End Date")
/* src/report/standard-reports/price-scatter.scm */
_("Step Size")
/* src/report/standard-reports/price-scatter.scm */
_("Price")
/* src/report/standard-reports/price-scatter.scm */
_("Report's currency")
/* src/report/standard-reports/price-scatter.scm */
_("Price of Commodity")
/* src/report/standard-reports/price-scatter.scm */
_("Price Source")
/* src/report/standard-reports/price-scatter.scm */
_("Invert prices")
/* src/report/standard-reports/price-scatter.scm */
_("Show Income/Expense")
/* src/report/standard-reports/price-scatter.scm */
_("Show Net Profit")
/* src/report/standard-reports/price-scatter.scm */
_("Show Asset & Liability bars")
/* src/report/standard-reports/price-scatter.scm */
_("Show Net Worth bars")
/* src/report/standard-reports/price-scatter.scm */
_("Marker")
/* src/report/standard-reports/price-scatter.scm */
_("Marker Color")
/* src/report/standard-reports/price-scatter.scm */
_("Plot Width")
/* src/report/standard-reports/price-scatter.scm */
_("Plot Height")
/* src/report/standard-reports/price-scatter.scm */
_("Calculate the price of this commodity.")
/* src/report/standard-reports/price-scatter.scm */
_("The source of price information")
/* src/report/standard-reports/price-scatter.scm */
_("Weighted Average")
/* src/report/standard-reports/price-scatter.scm */
_("The weighted average of all currency transactions of the past")
/* src/report/standard-reports/price-scatter.scm */
_("Actual Transactions")
/* src/report/standard-reports/price-scatter.scm */
_("The instantaneous price of actual currency transactions in the past")
/* src/report/standard-reports/price-scatter.scm */
_("Price Database")
/* src/report/standard-reports/price-scatter.scm */
_("The recorded prices")
/* src/report/standard-reports/price-scatter.scm */
_("Plot commodity per currency rather than currency per commodity.")
/* src/report/standard-reports/price-scatter.scm */
_("Color of the marker")
/* src/report/standard-reports/price-scatter.scm */
_("%s to %s")
/* src/report/standard-reports/price-scatter.scm */
_("Days")
/* src/report/standard-reports/price-scatter.scm */
_("Weeks")
/* src/report/standard-reports/price-scatter.scm */
_("Double-Weeks")
/* src/report/standard-reports/price-scatter.scm */
_("Months")
/* src/report/standard-reports/price-scatter.scm */
_("Years")
/* src/report/standard-reports/price-scatter.scm */
_("All Prices equal")
/* src/report/standard-reports/price-scatter.scm */
_("All the prices found are equal. This would result in a plot with one straight line. Unfortunately, the plotting tool can't handle that.")
/* src/report/standard-reports/price-scatter.scm */
_("All Prices at the same date")
/* src/report/standard-reports/price-scatter.scm */
_("All the prices found are from the same date. This would result in a plot with one straight line. Unfortunately, the plotting tool can't handle that.")
/* src/report/standard-reports/price-scatter.scm */
_("Only one price")
/* src/report/standard-reports/price-scatter.scm */
_("There was only one single price found for the selected commodities in the selected time period. This doesn't give a useful plot.")
/* src/report/standard-reports/price-scatter.scm */
_("No data")
/* src/report/standard-reports/price-scatter.scm */
_("There is no price information available for the selected commodities in the selected time period.")
/* src/report/standard-reports/price-scatter.scm */
_("Identical commodities")
/* src/report/standard-reports/price-scatter.scm */
_("Your selected commodity and the currency of the report are identical. It doesn't make sense to show prices for identical commodities.")
/* src/report/standard-reports/price-scatter.scm */
_("Price")
/* src/report/standard-reports/price-scatter.scm */
_("Price Scatterplot")
/* src/report/standard-reports/register.scm */
_("Date")
/* src/report/standard-reports/register.scm */
_("Num")
/* src/report/standard-reports/register.scm */
_("Description")
/* src/report/standard-reports/register.scm */
_("Memo")
/* src/report/standard-reports/register.scm */
_("Account")
/* src/report/standard-reports/register.scm */
_("Transfer")
/* src/report/standard-reports/register.scm */
_("Shares")
/* src/report/standard-reports/register.scm */
_("Lot")
/* src/report/standard-reports/register.scm */
_("Price")
/* src/report/standard-reports/register.scm */
_("Value")
/* src/report/standard-reports/register.scm */
_("Debit Value")
/* src/report/standard-reports/register.scm */
_("Credit Value")
/* src/report/standard-reports/register.scm */
_("Balance")
/* src/report/standard-reports/register.scm */
_("-- Split Transaction --")
/* src/report/standard-reports/register.scm */
_("Debit")
/* src/report/standard-reports/register.scm */
_("Credit")
/* src/report/standard-reports/register.scm */
_("General")
/* src/report/standard-reports/register.scm */
_("Title")
/* src/report/standard-reports/register.scm */
_("The title of the report")
/* src/report/standard-reports/register.scm */
_("Register Report")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Date")
/* src/report/standard-reports/register.scm */
_("Display the date?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Num")
/* src/report/standard-reports/register.scm */
_("Display the check number?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Description")
/* src/report/standard-reports/register.scm */
_("Display the description?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Memo")
/* src/report/standard-reports/register.scm */
_("Display the memo?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Account")
/* src/report/standard-reports/register.scm */
_("Display the account?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Shares")
/* src/report/standard-reports/register.scm */
_("Display the number of shares?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Lot")
/* src/report/standard-reports/register.scm */
_("Display the name of lot the shares are in?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Price")
/* src/report/standard-reports/register.scm */
_("Display the shares price?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Amount")
/* src/report/standard-reports/register.scm */
_("Display the amount?")
/* src/report/standard-reports/register.scm */
_("Single")
/* src/report/standard-reports/register.scm */
_("Single Column Display")
/* src/report/standard-reports/register.scm */
_("Double")
/* src/report/standard-reports/register.scm */
_("Two Column Display")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Value")
/* src/report/standard-reports/register.scm */
_("Display the value in transaction currency?")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Running Balance")
/* src/report/standard-reports/register.scm */
_("Display a running balance")
/* src/report/standard-reports/register.scm */
_("Display")
/* src/report/standard-reports/register.scm */
_("Totals")
/* src/report/standard-reports/register.scm */
_("Display the totals?")
/* src/report/standard-reports/register.scm */
_("Total Debits")
/* src/report/standard-reports/register.scm */
_("Total Credits")
/* src/report/standard-reports/register.scm */
_("Total Value Debits")
/* src/report/standard-reports/register.scm */
_("Total Value Credits")
/* src/report/standard-reports/register.scm */
_("Net Change")
/* src/report/standard-reports/register.scm */
_("Value Change")
/* src/report/standard-reports/register.scm */
_("Client")
/* src/report/standard-reports/register.scm */
_("Date")
/* src/report/standard-reports/register.scm */
_("Invoice")
/* src/report/standard-reports/register.scm */
_("Charge")
/* src/report/standard-reports/register.scm */
_("Amount")
/* src/report/standard-reports/register.scm */
_("Register")
/* src/report/standard-reports/sx-summary.scm */
_("Future Scheduled Transactions Summary")
/* src/report/standard-reports/sx-summary.scm */
_("Report Title")
/* src/report/standard-reports/sx-summary.scm */
_("Title for this report")
/* src/report/standard-reports/sx-summary.scm */
_("Company name")
/* src/report/standard-reports/sx-summary.scm */
_("Name of company/individual")
/* src/report/standard-reports/sx-summary.scm */
_("Start Date")
/* src/report/standard-reports/sx-summary.scm */
_("End Date")
/* src/report/standard-reports/sx-summary.scm */
_("Accounts")
/* src/report/standard-reports/sx-summary.scm */
_("Report on these accounts, if display depth allows.")
/* src/report/standard-reports/sx-summary.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/sx-summary.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/sx-summary.scm */
_("Depth limit behavior")
/* src/report/standard-reports/sx-summary.scm */
_("How to treat accounts which exceed the specified depth limit (if any)")
/* src/report/standard-reports/sx-summary.scm */
_("Parent account balances")
/* src/report/standard-reports/sx-summary.scm */
_("Parent account subtotals")
/* src/report/standard-reports/sx-summary.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/sx-summary.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/sx-summary.scm */
_("Omit zero balance figures")
/* src/report/standard-reports/sx-summary.scm */
_("Show blank space in place of any zero balances which would be shown")
/* src/report/standard-reports/sx-summary.scm */
_("Show accounting-style rules")
/* src/report/standard-reports/sx-summary.scm */
_("Use rules beneath columns of added numbers like accountants do")
/* src/report/standard-reports/sx-summary.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/sx-summary.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/sx-summary.scm */
_("Account Balance")
/* src/report/standard-reports/sx-summary.scm */
_("Show an account's balance")
/* src/report/standard-reports/sx-summary.scm */
_("Account Code")
/* src/report/standard-reports/sx-summary.scm */
_("Show an account's account code")
/* src/report/standard-reports/sx-summary.scm */
_("Account Type")
/* src/report/standard-reports/sx-summary.scm */
_("Show an account's account type")
/* src/report/standard-reports/sx-summary.scm */
_("Account Description")
/* src/report/standard-reports/sx-summary.scm */
_("Show an account's description")
/* src/report/standard-reports/sx-summary.scm */
_("Account Notes")
/* src/report/standard-reports/sx-summary.scm */
_("Show an account's notes")
/* src/report/standard-reports/sx-summary.scm */
_("Commodities")
/* src/report/standard-reports/sx-summary.scm */
_("Report's currency")
/* src/report/standard-reports/sx-summary.scm */
_("Price Source")
/* src/report/standard-reports/sx-summary.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/sx-summary.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/sx-summary.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/sx-summary.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/sx-summary.scm */
_("Recursive Balance")
/* src/report/standard-reports/sx-summary.scm */
_("Show the total balance, including balances in subaccounts, of any account at the depth limit")
/* src/report/standard-reports/sx-summary.scm */
_("Raise Accounts")
/* src/report/standard-reports/sx-summary.scm */
_("Shows accounts deeper than the depth limit at the depth limit")
/* src/report/standard-reports/sx-summary.scm */
_("Omit Accounts")
/* src/report/standard-reports/sx-summary.scm */
_("Disregard completely any accounts deeper than the depth limit")
/* src/report/standard-reports/sx-summary.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/sx-summary.scm */
_("Code")
/* src/report/standard-reports/sx-summary.scm */
_("Type")
/* src/report/standard-reports/sx-summary.scm */
_("Description")
/* src/report/standard-reports/sx-summary.scm */
_("Account title")
/* src/report/standard-reports/sx-summary.scm */
_("Balance")
/* src/report/standard-reports/sx-summary.scm */
_("Notes")
/* src/report/standard-reports/transaction.scm */
_("Transaction Report")
/* src/report/standard-reports/transaction.scm */
_("Sorting")
/* src/report/standard-reports/transaction.scm */
_("Primary Key")
/* src/report/standard-reports/transaction.scm */
_("Primary Subtotal")
/* src/report/standard-reports/transaction.scm */
_("Primary Subtotal for Date Key")
/* src/report/standard-reports/transaction.scm */
_("Secondary Key")
/* src/report/standard-reports/transaction.scm */
_("Secondary Subtotal")
/* src/report/standard-reports/transaction.scm */
_("Secondary Subtotal for Date Key")
/* src/report/standard-reports/transaction.scm */
_("Void Transactions")
/* src/report/standard-reports/transaction.scm */
_("Table for Exporting")
/* src/report/standard-reports/transaction.scm */
_("Common Currency")
/* src/report/standard-reports/transaction.scm */
_("Report's currency")
/* src/report/standard-reports/transaction.scm */
_("Split Transaction")
/* src/report/standard-reports/transaction.scm */
_("Total For ")
/* src/report/standard-reports/transaction.scm */
_("Grand Total")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Num")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Other Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Shares")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Price")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Running Balance")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Use Full Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Account Code")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Other Account Code")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Use Full Other Account Name")
/* src/report/standard-reports/transaction.scm */
_("Sorting")
/* src/report/standard-reports/transaction.scm */
_("Show Account Code")
/* src/report/standard-reports/transaction.scm */
_("Sorting")
/* src/report/standard-reports/transaction.scm */
_("Show Full Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/transaction.scm */
_("Num")
/* src/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/transaction.scm */
_("Account")
/* src/report/standard-reports/transaction.scm */
_("Transfer from/to")
/* src/report/standard-reports/transaction.scm */
_("Shares")
/* src/report/standard-reports/transaction.scm */
_("Price")
/* src/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/transaction.scm */
_("Debit")
/* src/report/standard-reports/transaction.scm */
_("Credit")
/* src/report/standard-reports/transaction.scm */
_("Balance")
/* src/report/standard-reports/transaction.scm */
_("Start Date")
/* src/report/standard-reports/transaction.scm */
_("End Date")
/* src/report/standard-reports/transaction.scm */
_("Style")
/* src/report/standard-reports/transaction.scm */
_("Report style")
/* src/report/standard-reports/transaction.scm */
_("Multi-Line")
/* src/report/standard-reports/transaction.scm */
_("Display N lines")
/* src/report/standard-reports/transaction.scm */
_("Single")
/* src/report/standard-reports/transaction.scm */
_("Display 1 line")
/* src/report/standard-reports/transaction.scm */
_("Convert all transactions into a common currency")
/* src/report/standard-reports/transaction.scm */
_("Formats the table suitable for cut & paste exporting with extra cells")
/* src/report/standard-reports/transaction.scm */
_("Accounts")
/* src/report/standard-reports/transaction.scm */
_("Report on these accounts")
/* src/report/standard-reports/transaction.scm */
_("Filter By...")
/* src/report/standard-reports/transaction.scm */
_("Filter on these accounts")
/* src/report/standard-reports/transaction.scm */
_("Filter Type")
/* src/report/standard-reports/transaction.scm */
_("Filter account")
/* src/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/transaction.scm */
_("Do not do any filtering")
/* src/report/standard-reports/transaction.scm */
_("Include Transactions to/from Filter Accounts")
/* src/report/standard-reports/transaction.scm */
_("Include transactions to/from filter accounts only")
/* src/report/standard-reports/transaction.scm */
_("Exclude Transactions to/from Filter Accounts")
/* src/report/standard-reports/transaction.scm */
_("Exclude transactions to/from all filter accounts")
/* src/report/standard-reports/transaction.scm */
_("How to handle void transactions")
/* src/report/standard-reports/transaction.scm */
_("Non-void only")
/* src/report/standard-reports/transaction.scm */
_("Show only non-voided transactions")
/* src/report/standard-reports/transaction.scm */
_("Void only")
/* src/report/standard-reports/transaction.scm */
_("Show only voided transactions")
/* src/report/standard-reports/transaction.scm */
_("Both")
/* src/report/standard-reports/transaction.scm */
_("Show both (and include void transactions in totals)")
/* src/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/transaction.scm */
_("Do not sort")
/* src/report/standard-reports/transaction.scm */
_("Account Name")
/* src/report/standard-reports/transaction.scm */
_("Sort & subtotal by account name")
/* src/report/standard-reports/transaction.scm */
_("Account Code")
/* src/report/standard-reports/transaction.scm */
_("Sort & subtotal by account code")
/* src/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/transaction.scm */
_("Sort by date")
/* src/report/standard-reports/transaction.scm */
_("Exact Time")
/* src/report/standard-reports/transaction.scm */
_("Sort by exact time")
/* src/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/transaction.scm */
_("Sort by the Reconciled Date")
/* src/report/standard-reports/transaction.scm */
_("Register Order")
/* src/report/standard-reports/transaction.scm */
_("Sort as with the register")
/* src/report/standard-reports/transaction.scm */
_("Other Account Name")
/* src/report/standard-reports/transaction.scm */
_("Sort by account transferred from/to's name")
/* src/report/standard-reports/transaction.scm */
_("Other Account Code")
/* src/report/standard-reports/transaction.scm */
_("Sort by account transferred from/to's code")
/* src/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/transaction.scm */
_("Sort by amount")
/* src/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/transaction.scm */
_("Sort by description")
/* src/report/standard-reports/transaction.scm */
_("Number")
/* src/report/standard-reports/transaction.scm */
_("Sort by check/transaction number")
/* src/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/transaction.scm */
_("Sort by memo")
/* src/report/standard-reports/transaction.scm */
_("Ascending")
/* src/report/standard-reports/transaction.scm */
_("smallest to largest, earliest to latest")
/* src/report/standard-reports/transaction.scm */
_("Descending")
/* src/report/standard-reports/transaction.scm */
_("largest to smallest, latest to earliest")
/* src/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/transaction.scm */
_("Weekly")
/* src/report/standard-reports/transaction.scm */
_("Weekly")
/* src/report/standard-reports/transaction.scm */
_("Monthly")
/* src/report/standard-reports/transaction.scm */
_("Monthly")
/* src/report/standard-reports/transaction.scm */
_("Quarterly")
/* src/report/standard-reports/transaction.scm */
_("Quarterly")
/* src/report/standard-reports/transaction.scm */
_("Yearly")
/* src/report/standard-reports/transaction.scm */
_("Yearly")
/* src/report/standard-reports/transaction.scm */
_("Sort by this criterion first")
/* src/report/standard-reports/transaction.scm */
_("Show Full Account Name")
/* src/report/standard-reports/transaction.scm */
_("Show the full account name for subtotals and subtitles?")
/* src/report/standard-reports/transaction.scm */
_("Show Account Code")
/* src/report/standard-reports/transaction.scm */
_("Show the account code for subtotals and subtitles?")
/* src/report/standard-reports/transaction.scm */
_("Subtotal according to the primary key?")
/* src/report/standard-reports/transaction.scm */
_("Do a date subtotal")
/* src/report/standard-reports/transaction.scm */
_("Primary Sort Order")
/* src/report/standard-reports/transaction.scm */
_("Order of primary sorting")
/* src/report/standard-reports/transaction.scm */
_("Sort by this criterion second")
/* src/report/standard-reports/transaction.scm */
_("Subtotal according to the secondary key?")
/* src/report/standard-reports/transaction.scm */
_("Do a date subtotal")
/* src/report/standard-reports/transaction.scm */
_("Secondary Sort Order")
/* src/report/standard-reports/transaction.scm */
_("Order of Secondary sorting")
/* src/report/standard-reports/transaction.scm */
_("Date")
/* src/report/standard-reports/transaction.scm */
_("Display the date?")
/* src/report/standard-reports/transaction.scm */
_("Reconciled Date")
/* src/report/standard-reports/transaction.scm */
_("Display the reconciled date?")
/* src/report/standard-reports/transaction.scm */
_("Num")
/* src/report/standard-reports/transaction.scm */
_("Display the check number?")
/* src/report/standard-reports/transaction.scm */
_("Description")
/* src/report/standard-reports/transaction.scm */
_("Display the description?")
/* src/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/transaction.scm */
_("Display the notes if the memo is unavailable?")
/* src/report/standard-reports/transaction.scm */
_("Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display the account name?")
/* src/report/standard-reports/transaction.scm */
_("Use Full Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display the full account name")
/* src/report/standard-reports/transaction.scm */
_("Account Code")
/* src/report/standard-reports/transaction.scm */
_("Display the account code")
/* src/report/standard-reports/transaction.scm */
_("Other Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display the other account name? (if this is a split transaction, this parameter is guessed).")
/* src/report/standard-reports/transaction.scm */
_("Use Full Other Account Name")
/* src/report/standard-reports/transaction.scm */
_("Display the full account name")
/* src/report/standard-reports/transaction.scm */
_("Other Account Code")
/* src/report/standard-reports/transaction.scm */
_("Display the other account code")
/* src/report/standard-reports/transaction.scm */
_("Shares")
/* src/report/standard-reports/transaction.scm */
_("Display the number of shares?")
/* src/report/standard-reports/transaction.scm */
_("Price")
/* src/report/standard-reports/transaction.scm */
_("Display the shares price?")
/* src/report/standard-reports/transaction.scm */
_("Running Balance")
/* src/report/standard-reports/transaction.scm */
_("Display a running balance")
/* src/report/standard-reports/transaction.scm */
_("Totals")
/* src/report/standard-reports/transaction.scm */
_("Display the totals?")
/* src/report/standard-reports/transaction.scm */
_("Memo")
/* src/report/standard-reports/transaction.scm */
_("Display the memo?")
/* src/report/standard-reports/transaction.scm */
_("Notes")
/* src/report/standard-reports/transaction.scm */
_("Amount")
/* src/report/standard-reports/transaction.scm */
_("Display the amount?")
/* src/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/transaction.scm */
_("No amount display")
/* src/report/standard-reports/transaction.scm */
_("Single")
/* src/report/standard-reports/transaction.scm */
_("Single Column Display")
/* src/report/standard-reports/transaction.scm */
_("Double")
/* src/report/standard-reports/transaction.scm */
_("Two Column Display")
/* src/report/standard-reports/transaction.scm */
_("Sign Reverses")
/* src/report/standard-reports/transaction.scm */
_("Reverse amount display for certain account types")
/* src/report/standard-reports/transaction.scm */
_("None")
/* src/report/standard-reports/transaction.scm */
_("Don't change any displayed amounts")
/* src/report/standard-reports/transaction.scm */
_("Income and Expense")
/* src/report/standard-reports/transaction.scm */
_("Reverse amount display for Income and Expense Accounts")
/* src/report/standard-reports/transaction.scm */
_("Credit Accounts")
/* src/report/standard-reports/transaction.scm */
_("Reverse amount display for Liability, Payable, Equity, Credit Card, and Income accounts")
/* src/report/standard-reports/transaction.scm */
_("From %s To %s")
/* src/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/transaction.scm */
_("Primary Subtotals/headings")
/* src/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/transaction.scm */
_("Secondary Subtotals/headings")
/* src/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/transaction.scm */
_("Grand Total")
/* src/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/transaction.scm */
_("Split Odd")
/* src/report/standard-reports/transaction.scm */
_("Colors")
/* src/report/standard-reports/transaction.scm */
_("Split Even")
/* src/report/standard-reports/transaction.scm */
_("Display")
/* src/report/standard-reports/transaction.scm */
_("Sign Reverses")
/* src/report/standard-reports/transaction.scm */
_("Style")
/* src/report/standard-reports/transaction.scm */
_("-- Split Transaction --")
/* src/report/standard-reports/transaction.scm */
_("No matching transactions found")
/* src/report/standard-reports/transaction.scm */
_("No transactions were found that match the time interval and account selection specified in the Options panel.")
/* src/report/standard-reports/trial-balance.scm */
_("Trial Balance")
/* src/report/standard-reports/trial-balance.scm */
_("Report Title")
/* src/report/standard-reports/trial-balance.scm */
_("Title for this report")
/* src/report/standard-reports/trial-balance.scm */
_("Company name")
/* src/report/standard-reports/trial-balance.scm */
_("Name of company/individual")
/* src/report/standard-reports/trial-balance.scm */
_("Start of Adjusting/Closing")
/* src/report/standard-reports/trial-balance.scm */
_("Date of Report")
/* src/report/standard-reports/trial-balance.scm */
_("Report variation")
/* src/report/standard-reports/trial-balance.scm */
_("Kind of trial balance to generate")
/* src/report/standard-reports/trial-balance.scm */
_("Accounts")
/* src/report/standard-reports/trial-balance.scm */
_("Report on these accounts")
/* src/report/standard-reports/trial-balance.scm */
_("Levels of Subaccounts")
/* src/report/standard-reports/trial-balance.scm */
_("Maximum number of levels in the account tree displayed")
/* src/report/standard-reports/trial-balance.scm */
_("Merchandising")
/* src/report/standard-reports/trial-balance.scm */
_("Gross adjustment accounts")
/* src/report/standard-reports/trial-balance.scm */
_("Do not net, but show gross debit/credit adjustments to these accounts. Merchandising businesses will normally select their inventory accounts here.")
/* src/report/standard-reports/trial-balance.scm */
_("Income summary accounts")
/* src/report/standard-reports/trial-balance.scm */
_("Adjustments made to these accounts are gross adjusted (see above) in the Adjustments, Adjusted Trial Balance, and Income Statement columns. Mostly useful for merchandising businesses.")
/* src/report/standard-reports/trial-balance.scm */
_("Entries")
/* src/report/standard-reports/trial-balance.scm */
_("Adjusting Entries pattern")
/* src/report/standard-reports/trial-balance.scm */
_("Any text in the Description column which identifies adjusting entries")
/* src/report/standard-reports/trial-balance.scm */
_("Adjusting Entries pattern is case-sensitive")
/* src/report/standard-reports/trial-balance.scm */
_("Causes the Adjusting Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/trial-balance.scm */
_("Adjusting Entries Pattern is regular expression")
/* src/report/standard-reports/trial-balance.scm */
_("Causes the Adjusting Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/trial-balance.scm */
_("Closing Entries pattern")
/* src/report/standard-reports/trial-balance.scm */
_("Any text in the Description column which identifies closing entries")
/* src/report/standard-reports/trial-balance.scm */
_("Closing Entries pattern is case-sensitive")
/* src/report/standard-reports/trial-balance.scm */
_("Causes the Closing Entries Pattern match to be case-sensitive")
/* src/report/standard-reports/trial-balance.scm */
_("Closing Entries Pattern is regular expression")
/* src/report/standard-reports/trial-balance.scm */
_("Causes the Closing Entries Pattern to be treated as a regular expression")
/* src/report/standard-reports/trial-balance.scm */
_("Include accounts with zero total balances")
/* src/report/standard-reports/trial-balance.scm */
_("Include accounts with zero total (recursive) balances in this report")
/* src/report/standard-reports/trial-balance.scm */
_("Display accounts as hyperlinks")
/* src/report/standard-reports/trial-balance.scm */
_("Shows each account in the table as a hyperlink to its register window")
/* src/report/standard-reports/trial-balance.scm */
_("Commodities")
/* src/report/standard-reports/trial-balance.scm */
_("Report's currency")
/* src/report/standard-reports/trial-balance.scm */
_("Price Source")
/* src/report/standard-reports/trial-balance.scm */
_("Show Foreign Currencies")
/* src/report/standard-reports/trial-balance.scm */
_("Display any foreign currency amount in an account")
/* src/report/standard-reports/trial-balance.scm */
_("Show Exchange Rates")
/* src/report/standard-reports/trial-balance.scm */
_("Show the exchange rates used")
/* src/report/standard-reports/trial-balance.scm */
_("General")
/* src/report/standard-reports/trial-balance.scm */
_("General")
/* src/report/standard-reports/trial-balance.scm */
_("Current Trial Balance")
/* src/report/standard-reports/trial-balance.scm */
_("Uses the exact balances in the general ledger")
/* src/report/standard-reports/trial-balance.scm */
_("Pre-adjustment Trial Balance")
/* src/report/standard-reports/trial-balance.scm */
_("Ignores Adjusting/Closing entries")
/* src/report/standard-reports/trial-balance.scm */
_("Work Sheet")
/* src/report/standard-reports/trial-balance.scm */
_("Creates a complete end-of-period work sheet")
/* src/report/standard-reports/trial-balance.scm */
_("Adjusting Entries")
/* src/report/standard-reports/trial-balance.scm */
_("Closing Entries")
/* src/report/standard-reports/trial-balance.scm */
_("for Period")
/* src/report/standard-reports/trial-balance.scm */
_("%s to %s")
/* src/report/standard-reports/trial-balance.scm */
_("For Period Covering %s to %s")
/* src/report/standard-reports/trial-balance.scm */
_("Trial Balance")
/* src/report/standard-reports/trial-balance.scm */
_("Adjustments")
/* src/report/standard-reports/trial-balance.scm */
_("Adjusted Trial Balance")
/* src/report/standard-reports/trial-balance.scm */
_("Income Statement")
/* src/report/standard-reports/trial-balance.scm */
_("Balance Sheet")
/* src/report/standard-reports/trial-balance.scm */
_("Debit")
/* src/report/standard-reports/trial-balance.scm */
_("Credit")
/* src/report/standard-reports/trial-balance.scm */
_("Account Name")
/* src/report/standard-reports/trial-balance.scm */
_("Unrealized Gains")
/* src/report/standard-reports/trial-balance.scm */
_("Unrealized Losses")
/* src/report/standard-reports/trial-balance.scm */
_("Net Income")
/* src/report/standard-reports/trial-balance.scm */
_("Net Loss")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Preparer")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Name of person preparing the report")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Name of organization or company prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Show preparer info")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Name of organization or company")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Enable hyperlinks in reports")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Background Tile")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Background tile for reports.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Heading Banner")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Heading Alignment")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Left")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Align the banner to the left")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Center")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Align the banner in the center")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Right")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Align the banner to the right")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Logo")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Company logo image.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General background color for report.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Text Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Normal body text color.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Link Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Link text color.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Default background for table cells.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Default alternate background for table cells.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Default color for subtotal rows.")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Color for subsubtotals")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Color for grand totals")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Space between table cells")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Preparer")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Show preparer info")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Text Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Link Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Background Tile")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Heading Banner")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Logo")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Heading Alignment")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Prepared by: ")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Prepared for: ")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Date: ")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Easy")
/* src/report/stylesheets/gnucash/report/stylesheet-easy.scm */
_("Easy")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Preparer")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Name of person preparing the report")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Name of organization or company prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Show preparer info")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Name of organization or company")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Enable hyperlinks in reports")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Background Tile")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Background tile for reports.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Heading Banner")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Heading Alignment")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Left")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Align the banner to the left")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Center")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Align the banner in the center")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Right")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Align the banner to the right")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Logo")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Company logo image.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General background color for report.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Text Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Normal body text color.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Link Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Link text color.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Default background for table cells.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Default alternate background for table cells.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Default color for subtotal rows.")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Color for subsubtotals")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Color for grand totals")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Space between table cells")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Preparer")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Show preparer info")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Text Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Link Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Background Tile")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Heading Banner")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Logo")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Heading Alignment")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Prepared by: ")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Prepared for: ")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Date: ")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Fancy")
/* src/report/stylesheets/gnucash/report/stylesheet-fancy.scm */
_("Technicolor")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Preparer")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Name of person preparing the report")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Name of organization or company prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Show preparer info")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Name of organization or company")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Enable hyperlinks in reports")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("String to be placed as a footer")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Background Tile")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Background tile for reports.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Heading Banner")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Banner for top of report.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Heading Alignment")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Banner for top of report.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Left")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Align the banner to the left")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Center")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Align the banner in the center")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Right")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Align the banner to the right")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Logo")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Company logo image.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General background color for report.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Text Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Normal body text color.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Link Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Link text color.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Default background for table cells.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Default alternate background for table cells.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Default color for subtotal rows.")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Color for subsubtotals")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Color for grand totals")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Space between table cells")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Preparer")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Prepared for")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Show preparer info")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Text Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Link Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Background Tile")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Heading Banner")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Logo")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Heading Alignment")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Prepared by: ")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Prepared for: ")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Date: ")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/gnucash/report/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Background Color")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Background color for reports.")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Background Pixmap")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Background tile for reports.")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("General")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Enable Links")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Enable hyperlinks in reports.")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Colors")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Background color for alternate lines.")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Table cell spacing")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Space between table cells")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Table cell padding")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Tables")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Table border width")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Plain")
/* src/report/stylesheets/gnucash/report/stylesheet-plain.scm */
_("Default")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Preparer")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Name of person preparing the report")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Prepared for")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Name of organization or company prepared for")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Show preparer info")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Name of organization or company")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Enable hyperlinks in reports")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Background Tile")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Background tile for reports.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Heading Banner")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Heading Alignment")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Left")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Align the banner to the left")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Center")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Align the banner in the center")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Right")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Align the banner to the right")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Logo")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Company logo image.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General background color for report.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Text Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Normal body text color.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Link Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Link text color.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Default background for table cells.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Default alternate background for table cells.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Default color for subtotal rows.")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Color for subsubtotals")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Color for grand totals")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Space between table cells")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Preparer")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Prepared for")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Show preparer info")
/* src/report/stylesheets/stylesheet-easy.scm */
_("General")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Text Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Link Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Background Tile")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Heading Banner")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Logo")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Heading Alignment")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Prepared by: ")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Prepared for: ")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Date: ")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Easy")
/* src/report/stylesheets/stylesheet-easy.scm */
_("Easy")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Preparer")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Name of person preparing the report")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Prepared for")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Name of organization or company prepared for")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Show preparer info")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Name of organization or company")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Enable hyperlinks in reports")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Background Tile")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Background tile for reports.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Heading Banner")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Heading Alignment")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Banner for top of report.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Left")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Align the banner to the left")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Center")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Align the banner in the center")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Right")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Align the banner to the right")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Logo")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Company logo image.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General background color for report.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Text Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Normal body text color.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Link Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Link text color.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Default background for table cells.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Default alternate background for table cells.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Default color for subtotal rows.")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Color for subsubtotals")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Color for grand totals")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Space between table cells")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Preparer")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Prepared for")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Show preparer info")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("General")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Text Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Link Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Background Tile")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Heading Banner")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Logo")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Images")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Heading Alignment")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Prepared by: ")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Prepared for: ")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Date: ")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Fancy")
/* src/report/stylesheets/stylesheet-fancy.scm */
_("Technicolor")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Preparer")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Name of person preparing the report")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Prepared for")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Name of organization or company prepared for")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Show preparer info")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Name of organization or company")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Enable hyperlinks in reports")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/stylesheet-footer.scm */
_("String to be placed as a footer")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Background Tile")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Background tile for reports.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Heading Banner")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Banner for top of report.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Heading Alignment")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Banner for top of report.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Left")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Align the banner to the left")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Center")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Align the banner in the center")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Right")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Align the banner to the right")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Logo")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Company logo image.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General background color for report.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Text Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Normal body text color.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Link Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Link text color.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Default background for table cells.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Default alternate background for table cells.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Default color for subtotal rows.")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Color for subsubtotals")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Color for grand totals")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Space between table cells")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Preparer")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Prepared for")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Show preparer info")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-footer.scm */
_("General")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Text Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Link Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Subheading/Subtotal Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Sub-subheading/total Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Grand Total Cell Color")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Background Tile")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Heading Banner")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Logo")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Images")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Heading Alignment")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Prepared by: ")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Prepared for: ")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Date: ")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/stylesheet-footer.scm */
_("Footer")
/* src/report/stylesheets/stylesheet-plain.scm */
_("General")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Background Color")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Background color for reports.")
/* src/report/stylesheets/stylesheet-plain.scm */
_("General")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Background Pixmap")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Background tile for reports.")
/* src/report/stylesheets/stylesheet-plain.scm */
_("General")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Enable Links")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Enable hyperlinks in reports.")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Colors")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Alternate Table Cell Color")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Background color for alternate lines.")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Table cell spacing")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Space between table cells")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Table cell padding")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Space between table cell edge and content")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Tables")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Table border width")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Bevel depth on tables")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Plain")
/* src/report/stylesheets/stylesheet-plain.scm */
_("Default")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Boolean Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a boolean option.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Multi Choice Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a multi choice option.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("First Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Help for first option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Second Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Help for second option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Third Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Help for third option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Fourth Options")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The fourth option rules!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("String Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a string option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Just a Date Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a date option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Time and Date Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a date option with time")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Combo Date Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a combination date option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Relative Date Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a relative date option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Number Option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a number option.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Background Color")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a color option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Text Color")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a color option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello Again")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("An account list option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is an account list option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello Again")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("A list option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a list option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The Good")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Good option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The Bad")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Bad option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The Ugly")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Ugly option")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Testing")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Crash the report")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is for testing. Your reports probably shouldn't have an option like this.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("This is a sample GnuCash report. See the guile (scheme) source code in the scm/report directory for details on writing your own reports, or extending existing reports.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("For help on writing reports, or to contribute your brand new, totally cool report, consult the mailing list %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("For details on subscribing to that list, see &lt;http://www.gnucash.org/&gt;.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("You can learn more about writing scheme at &lt;http://www.scheme.com/tspl2d/&gt;.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The current time is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The boolean option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("true")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("false")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The multi-choice option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The string option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The date option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The date and time option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The relative date option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The combination date option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The number option is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("The number option formatted as currency is %s.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Items you selected:")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("List items selected")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("(You selected no list items.)")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("You have selected no accounts.")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Have a nice day!")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Hello, World")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("Sample Report with Examples")
/* src/report/utility-reports/gnucash/report/hello-world.scm */
_("A sample report with examples.")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("General")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Number of columns")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Number of columns before wrapping to a new row")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("General")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Number of columns")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Report error")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("An error occurred while running the report.")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Edit Options")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Single Report")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Multicolumn View")
/* src/report/utility-reports/gnucash/report/view-column.scm */
_("Custom Multicolumn Report")
/* src/report/utility-reports/gnucash/report/welcome-to-gnucash.scm */
_("Welcome to GnuCash")
/* src/report/utility-reports/gnucash/report/welcome-to-gnucash.scm */
_("Welcome to GnuCash 2.4!")
/* src/report/utility-reports/gnucash/report/welcome-to-gnucash.scm */
_("GnuCash 2.4 has lots of nice features. Here are a few.")
/* src/report/utility-reports/gnucash/report/welcome-to-gnucash.scm */
_("Welcome to GnuCash")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Boolean Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a boolean option.")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Multi Choice Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a multi choice option.")
/* src/report/utility-reports/hello-world.scm */
_("First Option")
/* src/report/utility-reports/hello-world.scm */
_("Help for first option")
/* src/report/utility-reports/hello-world.scm */
_("Second Option")
/* src/report/utility-reports/hello-world.scm */
_("Help for second option")
/* src/report/utility-reports/hello-world.scm */
_("Third Option")
/* src/report/utility-reports/hello-world.scm */
_("Help for third option")
/* src/report/utility-reports/hello-world.scm */
_("Fourth Options")
/* src/report/utility-reports/hello-world.scm */
_("The fourth option rules!")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("String Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a string option")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Just a Date Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a date option")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Time and Date Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a date option with time")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Combo Date Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a combination date option")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Relative Date Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a relative date option")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Number Option")
/* src/report/utility-reports/hello-world.scm */
_("This is a number option.")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Background Color")
/* src/report/utility-reports/hello-world.scm */
_("This is a color option")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World!")
/* src/report/utility-reports/hello-world.scm */
_("Text Color")
/* src/report/utility-reports/hello-world.scm */
_("This is a color option")
/* src/report/utility-reports/hello-world.scm */
_("Hello Again")
/* src/report/utility-reports/hello-world.scm */
_("An account list option")
/* src/report/utility-reports/hello-world.scm */
_("This is an account list option")
/* src/report/utility-reports/hello-world.scm */
_("Hello Again")
/* src/report/utility-reports/hello-world.scm */
_("A list option")
/* src/report/utility-reports/hello-world.scm */
_("This is a list option")
/* src/report/utility-reports/hello-world.scm */
_("The Good")
/* src/report/utility-reports/hello-world.scm */
_("Good option")
/* src/report/utility-reports/hello-world.scm */
_("The Bad")
/* src/report/utility-reports/hello-world.scm */
_("Bad option")
/* src/report/utility-reports/hello-world.scm */
_("The Ugly")
/* src/report/utility-reports/hello-world.scm */
_("Ugly option")
/* src/report/utility-reports/hello-world.scm */
_("Testing")
/* src/report/utility-reports/hello-world.scm */
_("Crash the report")
/* src/report/utility-reports/hello-world.scm */
_("This is for testing. Your reports probably shouldn't have an option like this.")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World")
/* src/report/utility-reports/hello-world.scm */
_("This is a sample GnuCash report. See the guile (scheme) source code in the scm/report directory for details on writing your own reports, or extending existing reports.")
/* src/report/utility-reports/hello-world.scm */
_("For help on writing reports, or to contribute your brand new, totally cool report, consult the mailing list %s.")
/* src/report/utility-reports/hello-world.scm */
_("For details on subscribing to that list, see &lt;http://www.gnucash.org/&gt;.")
/* src/report/utility-reports/hello-world.scm */
_("You can learn more about writing scheme at &lt;http://www.scheme.com/tspl2d/&gt;.")
/* src/report/utility-reports/hello-world.scm */
_("The current time is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The boolean option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("true")
/* src/report/utility-reports/hello-world.scm */
_("false")
/* src/report/utility-reports/hello-world.scm */
_("The multi-choice option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The string option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The date option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The date and time option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The relative date option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The combination date option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The number option is %s.")
/* src/report/utility-reports/hello-world.scm */
_("The number option formatted as currency is %s.")
/* src/report/utility-reports/hello-world.scm */
_("Items you selected:")
/* src/report/utility-reports/hello-world.scm */
_("List items selected")
/* src/report/utility-reports/hello-world.scm */
_("(You selected no list items.)")
/* src/report/utility-reports/hello-world.scm */
_("You have selected no accounts.")
/* src/report/utility-reports/hello-world.scm */
_("Have a nice day!")
/* src/report/utility-reports/hello-world.scm */
_("Hello, World")
/* src/report/utility-reports/hello-world.scm */
_("Sample Report with Examples")
/* src/report/utility-reports/hello-world.scm */
_("A sample report with examples.")
/* src/report/utility-reports/view-column.scm */
_("General")
/* src/report/utility-reports/view-column.scm */
_("Number of columns")
/* src/report/utility-reports/view-column.scm */
_("Number of columns before wrapping to a new row")
/* src/report/utility-reports/view-column.scm */
_("General")
/* src/report/utility-reports/view-column.scm */
_("Number of columns")
/* src/report/utility-reports/view-column.scm */
_("Report error")
/* src/report/utility-reports/view-column.scm */
_("An error occurred while running the report.")
/* src/report/utility-reports/view-column.scm */
_("Edit Options")
/* src/report/utility-reports/view-column.scm */
_("Single Report")
/* src/report/utility-reports/view-column.scm */
_("Multicolumn View")
/* src/report/utility-reports/view-column.scm */
_("Custom Multicolumn Report")
/* src/report/utility-reports/welcome-to-gnucash.scm */
_("Welcome to GnuCash")
/* src/report/utility-reports/welcome-to-gnucash.scm */
_("Welcome to GnuCash 2.4!")
/* src/report/utility-reports/welcome-to-gnucash.scm */
_("GnuCash 2.4 has lots of nice features. Here are a few.")
/* src/report/utility-reports/welcome-to-gnucash.scm */
_("Welcome to GnuCash")
/* src/scm/command-line.scm */
_("A list of directories (strings) indicating where to look for html and parsed-html files. Each element must be a string representing a directory or a symbol where 'default expands to the default path, and 'current expands to the current value of the path.")
/* src/scm/command-line.scm */
_("Set the search path for documentation files")
/* src/scm/gnucash/price-quotes.scm */
_("No commodities marked for quote retrieval.")
/* src/scm/gnucash/price-quotes.scm */
_("No commodities marked for quote retrieval.")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/gnucash/price-quotes.scm */
_("You are missing some needed Perl libraries.""\n"
  "Run 'gnc-fq-update' as root to install them.")
/* src/scm/gnucash/price-quotes.scm */
_("You are missing some needed Perl libraries.""\n"
  "Run 'gnc-fq-update' as root to install them.")
/* src/scm/gnucash/price-quotes.scm */
_("There was a system error while retrieving the price quotes.")
/* src/scm/gnucash/price-quotes.scm */
_("There was a system error while retrieving the price quotes.")
/* src/scm/gnucash/price-quotes.scm */
_("There was an unknown error while retrieving the price quotes.")
/* src/scm/gnucash/price-quotes.scm */
_("There was an unknown error while retrieving the price quotes.")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to retrieve quotes for these items:")
/* src/scm/gnucash/price-quotes.scm */
_("Continue using only the good quotes?")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to retrieve quotes for these items:")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to retrieve quotes for these items:")
/* src/scm/gnucash/price-quotes.scm */
_("Continuing with good quotes.")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to create prices for these items:")
/* src/scm/gnucash/price-quotes.scm */
_("Add remaining good quotes?")
/* src/scm/gnucash/price-quotes.scm */
_("Unable to create prices for these items:")
/* src/scm/gnucash/price-quotes.scm */
_("Adding remaining good quotes.")
/* src/scm/main-window.scm */
_("Book Options")
/* src/scm/price-quotes.scm */
_("No commodities marked for quote retrieval.")
/* src/scm/price-quotes.scm */
_("No commodities marked for quote retrieval.")
/* src/scm/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/price-quotes.scm */
_("You are missing some needed Perl libraries.""\n"
  "Run 'gnc-fq-update' as root to install them.")
/* src/scm/price-quotes.scm */
_("You are missing some needed Perl libraries.""\n"
  "Run 'gnc-fq-update' as root to install them.")
/* src/scm/price-quotes.scm */
_("There was a system error while retrieving the price quotes.")
/* src/scm/price-quotes.scm */
_("There was a system error while retrieving the price quotes.")
/* src/scm/price-quotes.scm */
_("There was an unknown error while retrieving the price quotes.")
/* src/scm/price-quotes.scm */
_("There was an unknown error while retrieving the price quotes.")
/* src/scm/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/price-quotes.scm */
_("Unable to get quotes or diagnose the problem.")
/* src/scm/price-quotes.scm */
_("Unable to retrieve quotes for these items:")
/* src/scm/price-quotes.scm */
_("Continue using only the good quotes?")
/* src/scm/price-quotes.scm */
_("Unable to retrieve quotes for these items:")
/* src/scm/price-quotes.scm */
_("Unable to retrieve quotes for these items:")
/* src/scm/price-quotes.scm */
_("Continuing with good quotes.")
/* src/scm/price-quotes.scm */
_("Unable to create prices for these items:")
/* src/scm/price-quotes.scm */
_("Add remaining good quotes?")
/* src/scm/price-quotes.scm */
_("Unable to create prices for these items:")
/* src/scm/price-quotes.scm */
_("Adding remaining good quotes.")
/* src/tax/us/de_DE.scm */
_("Tax")
/* src/tax/us/de_DE.scm */
_("Tax Number")
/* src/tax/us/gnucash/tax/de_DE.scm */
_("Tax")
/* src/tax/us/gnucash/tax/de_DE.scm */
_("Tax Number")
/* src/tax/us/txf-de_DE.scm */
_("The electronic tax number of your business")
/* src/tax/us/txf.scm */
_("No help available.")
